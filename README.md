# GwtUtils

GwtUtils is a GWT library to speed up project development by reusing widgets, utilities, jQuery and Bootstrap.

## Installation

Download the project and compile it with Maven. It will then be available in your local Maven repository.

## Usage

Add the dependency to the pom.xml of your GWT project:
```xml
<dependency>
	<groupId>com.ebalde.gwtutils</groupId>
	<artifactId>gwt-utils</artifactId>
	<version>1.0</version>
</dependency>
```

Update the version accordingly.


Edit your project Module, e.g. MyProject.gwt.xml and Add the following inheritance:
```xml
<inherits name='com.ebalde.gwtutils.GwtUtils'/>
```
Now enjoy all the widgets and utilities available!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

The terms of the licenses of third-party libraries can be found in the file named THIRD-PARTY.txt.