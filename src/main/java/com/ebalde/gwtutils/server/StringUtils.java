package com.ebalde.gwtutils.server;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	/**
	 * Search for a word in a text by a part of it.
	 * 
	 * @param search the search string
	 * @param text   the text to search on
	 * @return a complete word that matches the search string
	 */
	public static String getWordFromText(String search, String text) {
		String completeFoundWord = "";
		String anyNoWordSplitter = "[^\\r\\n\\t\\f\\v,\\ \\(\\)\\:]*";
		Pattern p = Pattern.compile(anyNoWordSplitter + search + anyNoWordSplitter);
		Matcher m = p.matcher(text);
		if (m.find()) {
			completeFoundWord = m.group();
		}
		return completeFoundWord;
	}

	/**
	 * Convert a string to lower case and remove Diacritics (é->e, ç->c, etc.)
	 * 
	 * @param string the string to convert
	 * @return the string converted to lower case without diacritics
	 */
	public static String toLowerCaseRemoveDiacritics(String string) {
		return Normalizer.normalize(string.toLowerCase(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	}

}
