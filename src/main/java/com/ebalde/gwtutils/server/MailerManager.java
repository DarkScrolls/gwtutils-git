package com.ebalde.gwtutils.server;

import java.util.logging.Logger;

import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;

/**
 * Defines an Email Manager. It helps defining a {@link Mailer} for the application, which can than be reused to send
 * E-mails.
 * 
 * @author diogo
 *
 */
public abstract class MailerManager {
	static Logger logger = Logger.getLogger(MailerManager.class.getName());

	/**
	 * Send an email.
	 * 
	 * @param email
	 *            the email to send
	 */
	public void sendMail(Email email) {
		Mailer mailer = getMailer();
		mailer.sendMail(email);
	}

	/**
	 * Get the configured mailer for this application.
	 * 
	 * @return the configured mailer
	 */
	public abstract Mailer getMailer();

}
