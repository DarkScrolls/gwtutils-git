package com.ebalde.gwtutils.server;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ebalde.gwtutils.shared.UserSession;

public class SessionManager {

	/**
	 * Store a user in session.
	 * 
	 * @param userSession
	 *            the user who has signed in
	 * @param httpServletRequest
	 */
	public static void storeUserSession(UserSession userSession, HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession(true);
		session.setAttribute("user", userSession);
	}

	/**
	 * Get the user from the session - the user who is signed in.
	 * 
	 * @param httpServletRequest
	 * 
	 * @return the signed in user, or null if the session is no longer valid
	 */
	public static UserSession getUserSession(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		Object attribute = session.getAttribute("user");
		if (attribute != null && attribute instanceof UserSession) {
			return (UserSession) attribute;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @param httpServletRequest
	 */
	public static void invalidateUserSession(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		session.removeAttribute("user");
		session.invalidate();
	}

}
