package com.ebalde.gwtutils.server;

import javax.servlet.annotation.WebServlet;

import com.ebalde.gwtutils.client.UserSessionService;
import com.ebalde.gwtutils.shared.UserSession;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@WebServlet("/gwtutils/userSession")
public abstract class UserSessionServiceImpl extends RemoteServiceServlet implements UserSessionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8442297324826580682L;

	@Override
	public void logout() {
		SessionManager.invalidateUserSession(this.getThreadLocalRequest());
	}

	@Override
	public UserSession getUserSession() {
		return SessionManager.getUserSession(this.getThreadLocalRequest());
	}

}
