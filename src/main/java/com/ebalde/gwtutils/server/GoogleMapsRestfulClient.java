package com.ebalde.gwtutils.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;

import com.ebalde.gwtutils.server.ggeocoding.GeocodingApiResponse;
import com.google.gson.Gson;

public class GoogleMapsRestfulClient {
	static Logger logger = Logger.getLogger(GoogleMapsRestfulClient.class.getName());

	private static final String MAPS_GEOCODING_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=##address##&key=##apiKey##";

	/**
	 * Check if a String is a valid address, using Google Geocoding API.
	 * 
	 * @param address
	 *            the address to check
	 * @param googleGeocodingApiKey
	 *            the Geocoding API key to use
	 * @return
	 */
	public static boolean isAddressValid(String address, String googleGeocodingApiKey) {
		if (address != null && address.length() > 6 && address.contains(",")) {
			try {
				String encodedAddress = URLEncoder.encode(address, StandardCharsets.UTF_8.name());
				String url = MAPS_GEOCODING_URL.replace("##address##", encodedAddress).replace("##apiKey##",
						googleGeocodingApiKey);

				Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
				WebTarget webTarget = client.target(url);

				Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
				Response response = invocationBuilder.get();

				/*
				 * Using Gson instead of response.readEntity(SomeResponse.class) because it helped finding deserializing
				 * issues - wrong types on the auto generated classes. Classes generated using
				 * http://www.jsonschema2pojo.org/.
				 */
				String json = response.readEntity(String.class);

				Gson gson = new Gson();
				GeocodingApiResponse resp = gson.fromJson(json, GeocodingApiResponse.class);
				if (resp.getResults() != null && !resp.getResults().isEmpty()) {
					return true;
				} else {
					return false;
				}
			} catch (UnsupportedEncodingException e) {
				logger.severe("Could not encode the address: " + address + "error message: " + e.getMessage());
				return false;
			}
		} else {
			return false;
		}
	}
}
