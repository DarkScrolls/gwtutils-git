package com.ebalde.gwtutils.server;

import java.util.logging.Logger;

import com.ebalde.gwtutils.shared.AccountUtils;

public class ServerAccountManager {
	private static Logger logger = Logger.getLogger(ServerAccountManager.class.getName());

	/**
	 * Get the account validation link, where an email gets validated.
	 * 
	 * @param baseUrl
	 *            the website base URL
	 * @param email
	 *            the user email
	 * @param validationCode
	 *            the validation code
	 * @return
	 */
	public static String getAccountValidationUrl(String baseUrl, String email, String validationCode) {
		String url = baseUrl + "#" + AccountUtils.REGISTRATION_CONFIRMATION_PAGE + "/" + email + "/" + validationCode;
		logger.info("Account validation URL for " + email + ": " + url);
		return url;
	}

	/**
	 * Get the reset password link, where a user can reset the password
	 * 
	 * @param baseUrl
	 *            the website base URL
	 * @param email
	 *            the user email
	 * @param passwordResetCode
	 *            the validation code to reset the password
	 * @return
	 */
	public static String getResetPasswordUrl(String baseUrl, String email, String passwordResetCode) {
		String url = baseUrl + "#" + AccountUtils.PASSWORD_RESET_CONFIRMATION_PAGE + "/" + email + "/"
				+ passwordResetCode;
		logger.info("Password validation URL for " + email + ": " + url);
		return url;
	}

	/**
	 * Get the encrypted password, based on the original password
	 * 
	 * @param password
	 *            the original password
	 * @return the hashed password
	 */
	public static String getHashedPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	/**
	 * Generate an one-time password (OTP), a random alpha-numeric string.
	 * 
	 * @return an one-time password (OTP)
	 */
	public static String getRandomPassword() {
		PasswordGenerator passwordGenerator = new PasswordGenerator();
		return passwordGenerator.nextPassword();
	}
}
