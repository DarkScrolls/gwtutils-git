package com.ebalde.gwtutils.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Data from the user to be saved in the session
 * 
 * @author diogo
 *
 */
public class UserSession implements IsSerializable, Serializable {
	private int id;
	private String name;
	private String email;
	private Role role;

	private static final long serialVersionUID = 5080460977161260254L;

	public UserSession() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public UserSession(int id, String name, String email, Role role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserSession [id=" + id + ", name=" + name + ", email=" + email + ", role=" + role + "]";
	}

}
