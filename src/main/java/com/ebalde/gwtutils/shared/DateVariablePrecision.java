package com.ebalde.gwtutils.shared;

import java.sql.Date;

public class DateVariablePrecision {
	private Date date;
	private EnumDatePrecision enumDatePrecision;

	public DateVariablePrecision(Date date, EnumDatePrecision enumDatePrecision) {
		super();
		this.date = date;
		this.enumDatePrecision = enumDatePrecision;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public EnumDatePrecision getEnumDatePrecision() {
		return enumDatePrecision;
	}

	public void setEnumDatePrecision(EnumDatePrecision enumDatePrecision) {
		this.enumDatePrecision = enumDatePrecision;
	}

	public String getEnumDatePrecisionName() {
		return date == null || enumDatePrecision == null ? null : enumDatePrecision.name();
	}

}
