package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AuthenticationException extends Exception implements IsSerializable {
	
	public AuthenticationException() {
		super();
	}

	public AuthenticationException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
