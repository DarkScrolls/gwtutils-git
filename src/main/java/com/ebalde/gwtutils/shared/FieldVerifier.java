package com.ebalde.gwtutils.shared;

import com.ebalde.gwtutils.client.DateTimeUtils;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * <p>
 * FieldVerifier validates that the name the user enters is valid.
 * </p>
 * <p>
 * This class is in the <code>shared</code> package because we use it in both the client code and on the server. On the
 * client, we verify that the name is valid before sending an RPC request so the user doesn't have to wait for a network
 * round trip to get feedback. On the server, we verify that the name is correct to ensure that the input is correct
 * regardless of where the RPC originates.
 * </p>
 * <p>
 * When creating a class that is used on both the client and the server, be sure that all code is translatable and does
 * not use native JavaScript. Code that is not translatable (such as code that interacts with a database or the file
 * system) cannot be compiled into client-side JavaScript. Code that uses native JavaScript (such as Widgets) cannot be
 * run on the server.
 * </p>
 */
public class FieldVerifier implements IsSerializable {

	/**
	 * Verifies that the specified name is valid for our service.
	 * 
	 * @param name the name to validate
	 * @return true if valid, false if invalid
	 */
	public static boolean isValidName(String name) {
		return name.matches(EnumRegex.NAME.getRegex());
	}

	/**
	 * Verify if the given string is an email address
	 * 
	 * @param email the email address to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidEmail(String email) {
		return email.matches(EnumRegex.EMAIL.getRegex());
	}

	/**
	 * Verify if the given string is a valid password
	 * 
	 * @param password the password to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidPassword(String password) {
		if (password == null || password.isEmpty() || password.length() < 6 || password.length() > 100) {
			return false;
		}
		return true;
	}
	
	/**
	 * Verify if the given string is a strong password
	 * 
	 * @param password the password to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidStrongPassword(String password) {
		return password.matches(EnumRegex.STRONG_PASSWORD.getRegex());
	}

	/**
	 * Verify if the given string is a valid message
	 * 
	 * @param message the message to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidMessage(String message) {
		if (message == null || message.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Verify if the given string is a valid description
	 * 
	 * @param description the description to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidDescription(String description) {
		if (description.length() > 255) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the given string is a valid file name
	 * 
	 * @param filename the file name to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidFilename(String filename) {
		return filename.matches(EnumRegex.FILENAME.getRegex());
	}

	/**
	 * Check if a given address is valid. It does only some basic checks.
	 * 
	 * @param address the address to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidAddress(String address) {
		if (address != null && address.length() > 6 && address.contains(",")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if the given string is a valid phone number
	 * 
	 * @param phoneNumber the phone number to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidPhoneNumber(String phoneNumber) {
		return phoneNumber.matches(EnumRegex.PHONE_NUMBER.getRegex());
	}

	/**
	 * Check if a given string is a valid ISBN
	 * 
	 * @param isbn the isbn to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidIsbn(String isbn) {
		return isbn.matches(EnumRegex.ISBN.getRegex());
	}

	/**
	 * Check if a given string is a valid number
	 * 
	 * @param number the string to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidNumber(String number) {
		return number.matches(EnumRegex.NUMBER.getRegex());
	}
	
	/**
	 * Check if a given string is a valid float
	 * 
	 * @param float the string to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidFloat(String value) {
		return value.matches(EnumRegex.FLOAT.getRegex());
	}
	
	/**
	 * Check if a given string is a valid float with "." or ","
	 * 
	 * @param float the string to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidFloatDotComma(String value) {
		return value.matches(EnumRegex.FLOAT_DOT_COMMA.getRegex());
	}

	/**
	 * Check if a given string is a valid month (with digits)
	 * 
	 * @param string the string to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidMonthDigits(String string) {
		return string.matches(EnumRegex.MONTH_DIGITS.getRegex());
	}

	/**
	 * Check if a given string is a valid year (with digits)
	 * 
	 * @param string the string to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidYearDigits(String string) {
		return string.matches(EnumRegex.YEAR_DIGITS.getRegex());
	}

	/**
	 * Check if a short date String is a valid date
	 * 
	 * @param day   the day digits representation (e.g. '05')
	 * @param month the month digits representation (e.g. '11')
	 * @param year  the year digits representation (e.g. '2019')
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidDate(String day, String month, String year) {
		return isValidDate(year + "-" + month + "-" + day, DateTimeUtils.SHORT_DATE_FORMAT);
	}

	/**
	 * Check if a short date String representation is a valid date
	 * 
	 * @param dateString      a date representation (e.g. '2018-11-01')
	 * @param dateTimeFormat the date time format used to validate the date string e.g. DateTimeUtils.SHORT_DATE_FORMAT
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidDate(String dateString, String dateTimeFormat) {
		DateTimeFormat dtf = DateTimeFormat.getFormat(dateTimeFormat);
		try {
			dtf.parseStrict(dateString);
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the given string is a valid URL
	 * 
	 * @param url the url address to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidURL(String url) {
		return url.matches(EnumRegex.URL.getRegex());
	}
	
	/**
	 * Check if the given string is a valid PLZ
	 *
	 * @param plz the PLZ to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isValidPLZ(String plz) {
		return plz.matches(EnumRegex.PLZ.getRegex());
	}

    /**
     * Check if the given string is a valid PMID
     *
     * @param pmid the PMID to check
     * @return true if valid, false otherwise
     */
    public static boolean isValidPMID(String pmid) {
        return pmid.matches(EnumRegex.PMID.getRegex());
    }

    /**
     * Check if the given string is a valid DOI
     *
     * @param doi the DOI to check
     * @return true if valid, false otherwise
     */
    public static boolean isValidDOI(String doi) {
        return doi.matches(EnumRegex.DOI.getRegex());
    }

}
