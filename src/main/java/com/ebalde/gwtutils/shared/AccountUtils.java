package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AccountUtils implements IsSerializable {
	/**
	 * We need this page name in both client and server side (EnumPage cannot be used in the server side, because of the
	 * panels instantiation.
	 */
	public static final String REGISTRATION_CONFIRMATION_PAGE = "RegisterConf";

	/**
	 * We need this page name in both client and server side (EnumPage cannot be used in the server side, because of the
	 * panels instantiation.
	 */
	public static final String PASSWORD_RESET_CONFIRMATION_PAGE = "ResetPassword";

}
