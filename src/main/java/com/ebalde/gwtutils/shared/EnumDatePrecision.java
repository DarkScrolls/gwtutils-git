package com.ebalde.gwtutils.shared;

public enum EnumDatePrecision {
	DAY, MONTH, YEAR;

	public static EnumDatePrecision getByName(String name) {
		for (EnumDatePrecision e : values()) {
			if (e.name().equals(name)) {
				return e;
			}
		}
		return EnumDatePrecision.values()[0]; // by default return the first value (auto)
	}
}
