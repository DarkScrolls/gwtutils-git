package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ConflictException extends Exception implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6103354530949154905L;

}
