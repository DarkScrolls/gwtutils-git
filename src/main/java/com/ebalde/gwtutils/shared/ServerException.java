package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Specific Exception for GWT RPC. It implements IsSerializable and therefore it can be used without problems between
 * client and server side, unlike java Exception (which SQLException and others can extend from - this would trigger
 * exceptions).
 * 
 * @author diogo
 *
 */
public class ServerException extends Exception implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4846160129552145772L;

	public ServerException() {
		super();
	}

	public ServerException(String string) {
		super(string);
	}
}
