package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AccessDeniedException extends Exception implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4331399336375133191L;

}
