package com.ebalde.gwtutils.shared;

/**
 * Utility for the end project to implement an EnumRole enum. Than, it can be used with {@link UserSession}
 * 
 * @author diogo
 *
 */
public interface Role {
	public String name();
}