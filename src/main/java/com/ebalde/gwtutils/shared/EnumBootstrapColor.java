package com.ebalde.gwtutils.shared;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.gwt.user.client.Random;

public enum EnumBootstrapColor {
	PRIMARY, SECONDARY, SUCCESS, INFO, WARNING, DANGER, LIGHT, DARK, INDIGO, PURPLE, PINK, ORANGE, TEAL;

	private static final List<EnumBootstrapColor> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int BASE_COLORS_SIZE = 8;
	private static final int EXTRA_COLORS_SIZE = VALUES.size();

	public String getStyleClassName() {
		return this.name().toLowerCase();
	}

	public static EnumBootstrapColor getByName(String name) {
		for (EnumBootstrapColor e : values()) {
			if (e.name().toLowerCase().equals(name)) {
				return e;
			}
		}
		return EnumBootstrapColor.values()[0]; // by default return the first value
	}

	/**
	 * Get a random {@link EnumBootstrapColor}.
	 * 
	 * @param enableExtraColors
	 *            whether extra colors should be included in the random selection.
	 * @return random {@link EnumBootstrapColor}
	 */
	public static EnumBootstrapColor getRandom(boolean enableExtraColors) {
		int size = enableExtraColors ? EXTRA_COLORS_SIZE : BASE_COLORS_SIZE;
		return VALUES.get(Random.nextInt(size));
	}
}
