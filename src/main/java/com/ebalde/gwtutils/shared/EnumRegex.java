
package com.ebalde.gwtutils.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.regexp.shared.RegExp;

public enum EnumRegex implements IsSerializable {
	EMAIL("^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$"), // Email
	PHONE_NUMBER("^(\\+)?[0-9 -]{6,}$"), // Phone number regex
	NUMBER("^\\d+$"), // Number regex
	FLOAT("[-+]?[0-9]*\\.?[0-9]+"), // Float regex
	FLOAT_DOT_COMMA("[-+]?[0-9]*[\\.,]?[0-9]+"), // Float with dot or comma regex
	FILENAME("^[^<>:;,?\"*\\|/]+$"), // Filename regex
	ISBN("^((978[\\--– ])?[0-9][0-9\\--– ]{10}[\\--– ][0-9xX])|((978)?[0-9]{9}[0-9Xx])$"), // ISBN regex
	MONTH_DIGITS("^([1-9]|1[0-2]|0[1-9])$"), // Month digits regex
	YEAR_DIGITS("^\\d{4}$"), // Year digits regex
	URL("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"),
	NAME("^[A-Za-zÀ-ÖØ-öø-ÿ]+(?:[ '-][A-Za-zÀ-ÖØ-öø-ÿ]+)*$"), // Name regex
	PLZ("^[A-Za-z0-9\\s\\-]{3,10}$"), // Postal code regex
	PMID("^\\d{1,8}$"), // PMID regex
	DOI("^10\\.\\d{4,9}/[-._;()/:a-zA-Z0-9]+$"), // DOI regex
	STRONG_PASSWORD("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$"), // Strong password regex
	;

	private String regex;

	private EnumRegex(String regex) {
		this.regex = regex;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public boolean matches(String input) {
		RegExp regExp = RegExp.compile(regex);
		return regExp.test(input);
	}
}
