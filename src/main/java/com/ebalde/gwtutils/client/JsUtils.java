package com.ebalde.gwtutils.client;

import java.util.logging.Logger;

import com.ebalde.gwtutils.client.utils.JQuery;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import elemental2.dom.HTMLInputElement;

public class JsUtils {
	static Logger logger = Logger.getLogger(JsUtils.class.getName());

	/**
	 * Start the carousel slider.
	 */
	public static native void startCarousel() /*-{
		$wnd.jQuery('.carousel').carousel({
			interval : 6000
		});
	}-*/;

	/**
	 * Scroll the page to a target history token (e.g. for history token 'contact', the page will be scrolled to the
	 * '#contact' section.
	 * 
	 * @param historyToken the target history token
	 */
	public static native void scrollTo(String historyToken) /*-{
		$wnd.jQuery('html, body').animate({
			scrollTop : $wnd.jQuery("#" + historyToken).offset().top
		}, 1000, "easeInOutExpo");
	}-*/;

	/**
	 * Scroll instantaneously (no animation) the page to a target history token (e.g. for history token 'contact', the
	 * page will be scrolled to the '#contact' section.
	 * 
	 * @param historyToken the target history token
	 */
	public static native void jumpTo(String historyToken) /*-{
		$wnd.jQuery('html, body').animate({
			scrollTop : $wnd.jQuery("#" + historyToken).offset().top
		}, 0, "easeInOutExpo");
	}-*/;

	/**
	 * Remove style class from elements
	 * 
	 * @param elementStyleClass the style classes that identify the elements
	 * @param styleName         the style class to remove
	 */
	public static native void removeClassFromElements(String elementStyleClass, String styleName) /*-{
		$wnd.jQuery(elementStyleClass).removeClass(styleName);
	}-*/;

	/**
	 * Remove the html content from elements
	 * 
	 * @param elementStyleClass the style classes that identify the elements
	 */
	public static native void clearFromElements(String elementStyleClass) /*-{
		$wnd.jQuery(elementStyleClass).empty();
	}-*/;

	/**
	 * Add style class to elements
	 * 
	 * @param elementStyleClass the style classes that identify the elements
	 * @param styleName         the style class to add
	 */
	public static native void addClassToElements(String elementStyleClass, String styleName) /*-{
		$wnd.jQuery(elementStyleClass).addClass(styleName);
	}-*/;

	/**
	 * Add html to elements
	 * 
	 * @param elementStyleClass the style classes that identify the elements
	 * @param html              the HTML content to add
	 */
	public static native void addHtmlToElements(String elementStyleClass, String html) /*-{
		$wnd.jQuery(elementStyleClass).html(html);
	}-*/;

	/**
	 * Activate Select2 for a widget
	 * 
	 * @param widget         a {@link ListBox} for which Select2 should be activated
	 * @param dropDownParent the selector of the element for the dropdown to be appended to (e.g. ".modal"). This is
	 *                       useful when attempting to render Select2 correctly inside of modals and other small
	 *                       containers. If there is trouble using the search box inside a Bootstrap modal, for example,
	 *                       setting the dropdownParent option to the modal element solves the issue.
	 */
	public static void activateSelect2(Widget widget, String dropDownParent) {
		if (widget.getElement().getId() == null || widget.getElement().getId().isEmpty()) {
			widget.getElement().setId(HTMLPanel.createUniqueId());
		}
		activateSelect2("#" + widget.getElement().getId(), dropDownParent, false);
	}

	/**
	 * Activate Select2 for a widget
	 * 
	 * @param widget                  a {@link ListBox} for which Select2 should be activated
	 * @param dropDownParent          the selector of the element for the dropdown to be appended to (e.g. ".modal").
	 *                                This is useful when attempting to render Select2 correctly inside of modals and
	 *                                other small containers. If there is trouble using the search box inside a
	 *                                Bootstrap modal, for example, setting the dropdownParent option to the modal
	 *                                element solves the issue.
	 * @param isDynamicOptionCreation dynamically create new options from text input by the user in the search box?
	 */
	public static void activateSelect2(Widget widget, String dropDownParent, boolean isDynamicOptionCreation) {
		if (widget.getElement().getId() == null || widget.getElement().getId().isEmpty()) {
			widget.getElement().setId(HTMLPanel.createUniqueId());
		}
		activateSelect2("#" + widget.getElement().getId(), dropDownParent, isDynamicOptionCreation);
	}

	/**
	 * Activate Select2 for the element with the referenced selector.
	 * 
	 * @param selector                the selector of the element where select2 should be triggered (e.g. '#someId' or
	 *                                '.someClass')
	 * @param dropDownParent          the selector of the element for the dropdown to be appended to (e.g. ".modal").
	 *                                This is useful when attempting to render Select2 correctly inside of modals and
	 *                                other small containers. If there is trouble using the search box inside a
	 *                                Bootstrap modal, for example, setting the dropdownParent option to the modal
	 *                                element solves the issue.
	 * @param isDynamicOptionCreation dynamically create new options from text input by the user in the search box?
	 */
	public static native void activateSelect2(String selector, String dropDownParent, boolean isDynamicOptionCreation) /*-{
		$wnd.jQuery(selector).select2({
			theme : "bootstrap4", // bootstrap look
			dropdownParent : $wnd.jQuery(dropDownParent),
			tags : isDynamicOptionCreation,
			width : "100%"
		}).on('select2:select', function(e) {
			setTimeout(function() {
				$wnd.jQuery(selector + ' + span input').focus();
			}, 1); // only way found for the focus to be kept after selection
		});
	}-*/;

	public static native void updateURLWithoutReloading(String newUrl) /*-{
		$wnd.history.pushState(newUrl, "", newUrl);
	}-*/;

	/**
	 * Remove the set of matched elements from the DOM.
	 * 
	 * @param elementStyleClass the style class of the elements to be removed
	 */
	public static native void removeElements(String elementStyleClass) /*-{
		$wnd.jQuery(elementStyleClass).remove();
	}-*/;

	/**
	 * Run WOW animations
	 * 
	 */
	public static native void runWowAnimations() /*-{
		var wow = new $wnd.WOW();
		wow.init();
	}-*/;

	/**
	 * Enable all tooltips
	 */
	public static native void enableTooltips() /*-{
		$wnd.jQuery("[data-toggle='tooltip']").tooltip('enable');
	}-*/;

	/**
	 * Hide tooltips
	 */
	public static native void hideTooltips() /*-{
		$wnd.jQuery("[data-toggle='tooltip']").tooltip('hide');
	}-*/;

	/**
	 * Activates the jquery datepicker. Requires jquery-ui css and js.
	 * 
	 * @param selector   element(s) for which datepicker is activated
	 * @param dateFormat of the datepicker
	 * @see https://api.jqueryui.com/datepicker/#option-dateFormat
	 */
	public static void activateDatepicker(String selector, String dateFormat) {
		JQuery.Alias.$(selector).datepicker(); // activate datepicker
		JQuery.Alias.$(selector).datepicker("option", "dateFormat", dateFormat); // set date format
	}

	/**
	 * Activates the jquery datepicker. Requires jquery-ui css and js. This option, unlike
	 * {@link #activateDatepicker(String, String)}, allows to focus on the input element when the date widget is closed
	 * (because "this" cannot be called in JsInterop).
	 * 
	 * @param element    element for which datepicker is activated
	 * @param dateFormat of the datepicker
	 * @see https://api.jqueryui.com/datepicker/#option-dateFormat
	 */
	public static void activateDatepicker(HTMLInputElement element, String dateFormat) {
		JQuery.Alias.$(element).datepicker(); // activate datepicker
		JQuery.Alias.$(element).datepicker("option", "dateFormat", dateFormat); // set date format
		JQuery.Alias.$(element).datepicker("option", "onSelect", () -> element.focus()); // focus input element on close
	}

	/**
	 * Activates the jquery datepicker. Requires jquery-ui css and js. This option, unlike
	 * {@link #activateDatepicker(String, String)}, allows call a listener when a date is selected.
	 * 
	 * @param element        element for which datepicker is activated
	 * @param dateFormat     of the datepicker
	 * @param selectListener called when the item is selected
	 * @see https://api.jqueryui.com/datepicker/#option-dateFormat
	 */
	public static void activateDatepicker(HTMLInputElement element, String dateFormat, SelectListener selectListener) {
		JQuery.Alias.$(element).datepicker(); // Activate datepicker
		JQuery.Alias.$(element).datepicker("option", "dateFormat", dateFormat); // Set date format

		JQuery.Alias.$(element).datepicker("option", "onSelect", () -> {
			element.focus(); // Focus input element on close
			selectListener.onSelect(); // Call select listener
		});
	}
}
