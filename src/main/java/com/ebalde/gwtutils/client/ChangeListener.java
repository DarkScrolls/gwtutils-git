package com.ebalde.gwtutils.client;

/**
 * Simple listener. Can be used in different contexts.
 * 
 * @author diogo
 *
 */
public interface ChangeListener {
	/**
	 * Triggered when a change is made
	 */
	public void onChange();
}
