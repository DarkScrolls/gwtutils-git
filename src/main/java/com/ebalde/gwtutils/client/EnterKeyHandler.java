package com.ebalde.gwtutils.client;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;

/**
 * Detect when ENTER is pressed. The {@link KeyUpEvent} is used because the {@link KeyPressHandler} does not work in all
 * browsers (e.g. Firefox).
 * 
 * @author diogo
 *
 */
public abstract class EnterKeyHandler implements KeyUpHandler {

	@Override
	public void onKeyUp(KeyUpEvent event) {
		if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
			enterKeyUp(event);
		}
	}

	public abstract void enterKeyUp(KeyUpEvent event);
}