/**
 * 
 */
package com.ebalde.gwtutils.client;

import com.ebalde.gwtutils.client.panels.CloseableModal;
import com.ebalde.gwtutils.client.panels.HTMLModal;
import com.ebalde.gwtutils.client.panels.SimpleModal;
import com.ebalde.gwtutils.client.utils.BootstrapModal;
import com.ebalde.gwtutils.client.utils.JQuery;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLElement;

/**
 * Manages GWT {@link DialogBox} and Bootstrap Modal and Popover
 * 
 * @author diogo
 * 
 */
public class PopupManager {

	private static DialogBox dialogBox = new DialogBoxExtended(true);

	private static CloseableModal closeableModal = new CloseableModal();
	private static SimpleModal simpleModal = new SimpleModal();
	private static SimpleModal simpleStaticModal = new SimpleModal();
	private static String simpleModalDefaultStyle = simpleModal.getStyleName();

	private static HTMLModal htmlModal = new HTMLModal();

	private static HTMLModal staticModal = new HTMLModal();

	/**
	 * Open a popup with the desired widget inside.
	 * 
	 * @param dialogContents the content of the dialog box
	 * 
	 * @return the new dialog box
	 */
	public static void openDialogBox(Widget dialogContents) {
		dialogBox.setWidget(dialogContents);

		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.setModal(false); // to allow ctrl + c to copy text

		dialogBox.setHeight("30px");
		dialogBox.setWidth("100%");
		// grid is important, so that it works as expected also on IE 11
		dialogBox.setStyleName("dialogBox col-md-10 col-lg-8 col-xl-6 border border-light");

		dialogBox.center();
		dialogBox.show();
	}

	/**
	 * Closes the dialog box
	 */
	public static void closeDialogBox() {
		dialogBox.hide();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog) with close buttons already built-in.
	 * 
	 * @param modalContent the widget to add to the modal content area
	 */
	public static void openCloseableModal(Widget modalContent) {
		closeableModal.setContent(modalContent);
		closeableModal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog).
	 * 
	 * @param modalContent the widget to add to the modal content area
	 */
	public static void openModal(Widget modalContent) {
		simpleModal.setStyleName(simpleModalDefaultStyle);
		simpleModal.setContent(modalContent);
		simpleModal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog) with options.
	 * 
	 * @param modalContent the widget to add to the modal content area
	 * @param options      options for configuring the modal
	 */
	public static void openStaticModal(Widget modalContent) {
		simpleStaticModal.setStyleName(simpleModalDefaultStyle);
		simpleStaticModal.setContent(modalContent);
		
		// get the simpleStaticModal element
		Element element = DomGlobal.document.querySelector("#" + simpleStaticModal.getElement().getId());

		BootstrapModal.Options options = new BootstrapModal.Options();
		options.backdrop = "static";
		options.keyboard = true;
		BootstrapModal modal = new BootstrapModal(element, options);
		modal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog).
	 * 
	 * @param modalContent the widget to add to the modal content area
	 */
	public static void openModal(HTMLElement modalContent) {
		htmlModal.setContent(modalContent);
		htmlModal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog) with options.
	 * 
	 * @param modalContent the element to add to the modal content area
	 * @param options      options for configuring the modal
	 */
	public static void openStaticModal(HTMLElement modalContent) {
		staticModal.setContent(modalContent);

		BootstrapModal.Options options = new BootstrapModal.Options();
		options.backdrop = "static";
		options.keyboard = true;
		BootstrapModal modal = new BootstrapModal(staticModal.getElemental2(), options);
		modal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog).
	 * 
	 * @param modalContent the widget to add to the modal content area
	 * @param styleName    to add to the modal
	 */
	public static void openModal(Widget modalContent, String styleName) {
		simpleModal.setStyleName("modal");
		simpleModal.addStyleName(styleName);
		simpleModal.setContent(modalContent);
		simpleModal.show();
	}

	/**
	 * Open a Bootstrap modal (not the GWT dialog).
	 * 
	 * @param modalContent the element to add to the modal content area
	 * @param style        class to add to the modal
	 */
	public static void openModal(HTMLElement modalContent, String styleName) {
		htmlModal.setStyleName("modal");
		htmlModal.addStyleName(styleName);
		htmlModal.setContent(modalContent);
		htmlModal.show();
	}

	public static void closeModal() {
		JQuery.Alias.$(".modal").modal("hide");
	}
}
