package com.ebalde.gwtutils.client;

/**
 * Simple click listener. Can be used in different contexts.
 * 
 * @author diogo
 *
 */
public interface ClickListener {
	/**
	 * Triggered when an item is clicked
	 */
	public void onClick(String itemId);
}
