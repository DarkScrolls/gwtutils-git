package com.ebalde.gwtutils.client;

public interface RemoveListener {
	/**
	 * Triggered when an item is removed
	 */
	public void onRemove();

}
