package com.ebalde.gwtutils.client;

/**
 * Change listener that allows to return a string. Useful to add an ID do the callbck, for example.
 * 
 * @author diogo
 *
 */
public interface ChangeListenerExtended {

	/**
	 * Triggered when a change is made, allows a value callback
	 */
	public void onChange(String value);
}