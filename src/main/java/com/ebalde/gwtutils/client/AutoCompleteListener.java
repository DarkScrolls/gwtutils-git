package com.ebalde.gwtutils.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Request listener.
 * 
 * @author diogo
 *
 */
public interface AutoCompleteListener {
	/**
	 * Get the search suggestions for the autocomplete.
	 * 
	 * @param search        the text in the search input
	 * @param asyncCallback asynchronous callback with the list of suggestion strings
	 */
	public void getSearchSuggestions(String search, AsyncCallback<ArrayList<String>> asyncCallback);
}
