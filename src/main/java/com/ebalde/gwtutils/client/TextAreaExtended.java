package com.ebalde.gwtutils.client;

import com.google.gwt.user.client.ui.TextArea;

public class TextAreaExtended extends TextArea {
	
	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}
	
	public void setAttRows(String rows) {
		this.getElement().setAttribute("rows", rows);
	}

	public void setPlaceholder(String placeholder) {
		this.getElement().setAttribute("placeholder", placeholder);
	}
	
	public void setType(String type) {
		this.getElement().setAttribute("type", type);
	}

	public void setAriaDescribedBy(String ariaDescribedBy) {
		this.getElement().setAttribute("aria-describedby", ariaDescribedBy);		
	}
}