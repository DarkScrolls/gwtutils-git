package com.ebalde.gwtutils.client;

import com.ebalde.gwtutils.shared.AuthenticationException;
import com.ebalde.gwtutils.shared.UserSession;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("userSession")
public interface UserSessionService extends RemoteService {
	UserSession login(String email, String password) throws AuthenticationException;

	void logout();
	
	UserSession getUserSession();
}