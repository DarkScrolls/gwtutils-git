package com.ebalde.gwtutils.client;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;

public class SuggestOracleExtended extends MultiWordSuggestOracle {
	private AutoCompleteListener autoCompleteListener;

	@Override
	public void requestSuggestions(Request request, Callback callback) {
		ArrayList<MultiWordSuggestion> filteredSuggestions = new ArrayList<>();

		autoCompleteListener.getSearchSuggestions(request.getQuery(), new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onSuccess(ArrayList<String> result) {
				int suggestionsNumber = 0;
				String search = request.getQuery();
				// add formatted suggestions
				for (String suggestion : result) {
					filteredSuggestions.add(new MultiWordSuggestion(suggestion, getDisplayString(search, suggestion)));
					if (++suggestionsNumber > 10) {
						break;
					}
				}
				Response response = new Response(filteredSuggestions);
				callback.onSuggestionsReady(request, response);
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	private static String getDisplayString(String search, String strToFormat) {
		String[] searchWords = search.split("\\s");
		Arrays.sort(searchWords, new java.util.Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s2.length() - s1.length();// comparison
			}
		}); // long strings should match first - match "word" instead of "wo" if both are in the searchWords
		String pattern = "(";
		for (int i = 0; i < searchWords.length; i++) {
			pattern += searchWords[i];
			if (i < searchWords.length - 1) { // not the last
				pattern += "|";
			}
		}
		pattern += ")";
		return strToFormat.toLowerCase() // case insensitive
				.replaceAll(pattern.toLowerCase(), "<strong class=\"bg-light\">$1</strong>");
	}

	public AutoCompleteListener getAutoCompleteListener() {
		return autoCompleteListener;
	}

	public void setAutoCompleteListener(AutoCompleteListener autoCompleteListener) {
		this.autoCompleteListener = autoCompleteListener;
	}

	public static void main(String[] args) {
		System.out.println(getDisplayString("aventura uma", "Uma Aventura no Comboio"));
		System.out.println(getDisplayString("uma conímbriga", "Uma aventura em Conímbriga"));
		System.out.println(getDisplayString("uma conimbriga", "Uma aventura em Conímbriga"));
		System.out.println(getDisplayString("coração do líder de criança", "Pastoreando o Coração do Líder de Crianças"));
	}

}
