package com.ebalde.gwtutils.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.StyleInjector;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GwtUtils implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		StyleInjector.inject(CssResources.INSTANCE.modalCss().getText());
		StyleInjector.inject(CssResources.INSTANCE.gwtUtils().getText());
	}
}
