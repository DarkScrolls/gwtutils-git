package com.ebalde.gwtutils.client.page;

import com.google.gwt.user.client.ui.UIObject;

public interface Page {

	UIObject getPanel();

	String getToken();

	boolean isAuthNeeded();
}
