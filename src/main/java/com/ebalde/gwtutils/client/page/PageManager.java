package com.ebalde.gwtutils.client.page;

import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;

/**
 * Manages the history tokens and page loading.
 * 
 * @author diogo
 *
 */
public abstract class PageManager implements EntryPoint, ValueChangeHandler<String> {
	private static final Logger logger = Logger.getLogger(PageManager.class.getName());

	public void onModuleLoad() {
		History.addValueChangeHandler(this);
		loadPage(History.getToken());
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		logger.info("history token: " + event.getValue());
		loadPage(event.getValue());
	}

	/**
	 * Load a page based on a menu name - URL, direct link
	 * 
	 * @param historyToken
	 *            the history token from the URL
	 */
	protected void loadPage(String historyToken) {
		logger.info("Loading page '" + historyToken + "'");

		Page page = getPage(HistoryTokenManager.getPageName(historyToken));
		if (page == null) {
			page = getHome();
		}
		loadPage(page);
	}

	/**
	 * Get a page from its name
	 * 
	 * @param pageName
	 *            the page name to search (e.g. Contact)
	 * @return the page enum
	 */
	protected Page getPage(String pageName) {
		for (Page page : getEnumPages()) {
			if (pageName.compareTo(page.getToken()) == 0) {
				return page;
			}
		}
		return null;
	}

	/**
	 * Get all the pages
	 * 
	 * @return the pages
	 */
	public abstract Page[] getEnumPages();

	/**
	 * Get the home page
	 * 
	 * @return the home page
	 */
	public abstract Page getHome();

	/**
	 * Load a specific page
	 * 
	 * @param page
	 *            the page to load
	 */
	public abstract void loadPage(Page page);

	/**
	 * Go to another page.
	 * 
	 * @param page
	 *            the page to go to
	 */
	public static void goTo(Page page) {
		goTo(page.getToken());
	}

	/**
	 * Go to another page. Loads the menu and content
	 * 
	 * @param token
	 *            the token to go to (e.g. page-top)
	 */
	public static void goTo(String token) {
		History.newItem(token);
	}

}
