package com.ebalde.gwtutils.client.page;

import com.google.gwt.user.client.History;

/**
 * Manages the history tokens e.g. www.myurl.com#Search/mySearchInput
 * 
 * @author diogo
 *
 */
public class HistoryTokenManager {

	/**
	 * Get the page from the history token. In www.myurl.com#Search/mySearchInput/otherInput, 'Search' would be the
	 * page.
	 * 
	 * @param historyToken
	 *            the history token
	 * @return the page name
	 */
	public static String getPageName(String historyToken) {
		String pageName = "";
		if (historyToken != null && !historyToken.isEmpty()) {
			pageName = historyToken.split("/")[0];
		}
		return pageName;
	}

	/**
	 * Get the page from the history token obtained from the current URL.
	 * 
	 * @return the page name
	 */
	public static String getPageName() {
		return getPageName(History.getToken());
	}

	/**
	 * Get the string following the first "/" of the history token. Can contain "/".
	 * 
	 * @param historyToken
	 * @return the history token string following "/"
	 */
	public static String getSingleParameter(String historyToken) {
		String parameter = null;
		if (historyToken != null && !historyToken.isEmpty() && historyToken.contains("/")) {
			parameter = historyToken.substring(historyToken.indexOf("/") + 1);
		}
		return parameter;
	}

	/**
	 * Get the string following the first "/" of the history token obtained from the current URL.
	 * 
	 * @return the history token string following "/"
	 */
	public static String getSingleParameter() {
		return getSingleParameter(History.getToken());
	}

	/**
	 * Get a part of the history token, provided its parts are split by "/"
	 * 
	 * @param historyToken
	 *            the complete history token
	 * @param index
	 *            the part to return
	 * @return the requested part of the history token, or null if it does not exist
	 */
	public static String getParameter(String historyToken, int index) {
		String parameter = null;
		if (historyToken != null && !historyToken.isEmpty() && historyToken.contains("/")) {
			String[] parts = historyToken.split("/");
			if (parts.length > index) {
				return parts[index];
			}
		}
		return parameter;
	}

	/**
	 * Get a part of the history token obtained from the current URL, provided its parts are split by "/"
	 * 
	 * @param index
	 *            the part to return
	 * @return the requested part of the history token from the current URL, or null if it does not exist
	 */
	public static String getParameter(int index) {
		return getParameter(History.getToken(), index);
	}

}
