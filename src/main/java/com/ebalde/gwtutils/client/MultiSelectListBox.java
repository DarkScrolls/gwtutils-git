package com.ebalde.gwtutils.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.ListBox;

/**
 * Extends Listbox features with multiple select. Listbox does not allow getting multiple selected items or values.
 * 
 * @author diogo
 *
 */
public class MultiSelectListBox extends ListBox {
	
	public MultiSelectListBox() {
		this.setMultipleSelect(true);
	}

	public String getAttId() {
		return this.getElement().getAttribute("id");
	}

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}

	/**
	 * Get the selected indexes
	 * 
	 * @return the selected indexes
	 */
	public List<Integer> getSelectedIndexes() {
		ArrayList<Integer> selectedItems = new ArrayList<Integer>();
		for (int i = 0; i < getItemCount(); i++) {
			if (isItemSelected(i)) {
				selectedItems.add(i);
			}
		}
		return selectedItems;
	}

	/**
	 * Get the selected items (items text)
	 * 
	 * @return the selected items (items text)
	 */
	public List<String> getSelectedItemsText() {
		ArrayList<String> selectedItems = new ArrayList<String>();
		for (int i = 0; i < getItemCount(); i++) {
			if (isItemSelected(i)) {
				selectedItems.add(getItemText(i));
			}
		}
		return selectedItems;
	}

	/**
	 * Get the selected values
	 * 
	 * @return the selected values
	 */
	public List<String> getSelectedValues() {
		ArrayList<String> selectedItems = new ArrayList<String>();
		for (int i = 0; i < getItemCount(); i++) {
			if (isItemSelected(i)) {
				selectedItems.add(getValue(i));
			}
		}
		return selectedItems;
	}
}
