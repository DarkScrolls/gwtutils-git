package com.ebalde.gwtutils.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

public class InputValidator {

	/**
	 * Check if the input text is valid. If not, it adds the style name is-invalid to the input - error message is shown
	 * automatically. If it is valid, the error visual hints are removed from the input.
	 * 
	 * @param widget  the input
	 * @param isValid whether the input is valid or not.
	 */
	public static void assertInputValid(Widget widget, boolean isValid) {
		if (!isValid) {
			widget.addStyleName("is-invalid");
		} else {
			widget.removeStyleName("is-invalid");
		}
	}

	/**
	 * Check if the input text is valid. If not, it adds the style name is-invalid to the input - error message is shown
	 * automatically. If it is valid, the error visual hints are removed from the input.
	 * 
	 * @param element the input
	 * @param isValid whether the input is valid or not.
	 */
	public static void assertInputValid(Element element, boolean isValid) {
		if (!isValid) {
			element.addClassName("is-invalid");
		} else {
			element.removeClassName("is-invalid");
		}
	}

	/**
	 * Check if the input text is valid. If not, it adds the style name is-invalid to the input - error message is shown
	 * automatically. If it is valid, the error visual hints are removed from the input.
	 * 
	 * @param element the input
	 * @param isValid whether the input is valid or not.
	 */
	public static void assertInputValid(elemental2.dom.Element element, boolean isValid) {
		if (!isValid) {
			element.classList.add("is-invalid");
		} else {
			element.classList.remove("is-invalid");
		}
	}
}
