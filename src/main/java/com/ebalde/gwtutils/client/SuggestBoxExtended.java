package com.ebalde.gwtutils.client;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.SuggestBox;

public class SuggestBoxExtended extends SuggestBox {
	private String placeholder;

	@UiConstructor
	public SuggestBoxExtended() {
		this(new SuggestOracleExtended());
	}

	public SuggestBoxExtended(SuggestOracleExtended oracle) {
		super(oracle);
		this.setAutoSelectEnabled(false);
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
		this.getElement().setAttribute("placeholder", placeholder);
	}
	
	@Override
	public SuggestOracleExtended getSuggestOracle() {
		return (SuggestOracleExtended) super.getSuggestOracle();
	}
}