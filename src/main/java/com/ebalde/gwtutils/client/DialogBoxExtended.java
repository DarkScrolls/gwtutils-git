package com.ebalde.gwtutils.client;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * {@link DialogBox} that closes on Escape pressed.
 * 
 * @author diogo
 *
 */
public class DialogBoxExtended extends DialogBox {
	/**
	 * Creates an empty dialog box. It should not be shown until its child widget has been added using
	 * {@link #add(Widget)}.
	 */
	public DialogBoxExtended() {
		super(false);
	}

	/**
	 * Creates an empty dialog box specifying its "auto-hide" property. It should not be shown until its child widget
	 * has been added using {@link #add(Widget)}.
	 *
	 * @param autoHide
	 *            <code>true</code> if the dialog should be automatically hidden when the user clicks outside of it
	 */
	public DialogBoxExtended(boolean autoHide) {
		super(autoHide, true);
	}

	/**
	 * Creates an empty dialog box specifying its {@link Caption}. It should not be shown until its child widget has
	 * been added using {@link #add(Widget)}.
	 *
	 * @param captionWidget
	 *            the widget that is the DialogBox's header.
	 */
	public DialogBoxExtended(Caption captionWidget) {
		super(false, true, captionWidget);
	}

	/**
	 * Creates an empty dialog box specifying its "auto-hide" and "modal" properties. It should not be shown until its
	 * child widget has been added using {@link #add(Widget)}.
	 *
	 * @param autoHide
	 *            <code>true</code> if the dialog should be automatically hidden when the user clicks outside of it
	 * @param modal
	 *            <code>true</code> if keyboard and mouse events for widgets not contained by the dialog should be
	 *            ignored
	 */
	public DialogBoxExtended(boolean autoHide, boolean modal) {
		super(autoHide, modal, new CaptionImpl());
	}

	@Override
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		// on escape pressed, close the dialog box
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				hide();
			}
			break;
		}
	}

}
