package com.ebalde.gwtutils.client;

import java.sql.Timestamp;
import java.util.Date;

import com.ebalde.gwtutils.shared.EnumDatePrecision;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class DateTimeUtils {

	public static final String DISPLAY_DATE_FORMAT = "d MMM yyyy";

	public static final String SHORT_DATE_FORMAT = "yyyy-MM-dd";

	public static final String TIME_SHORT_FORMAT = "HH:mm";

	/**
	 * Convert a {@link Date} into a time String.
	 * 
	 * @param date the date to convert
	 * @return the time String
	 */
	public static String getTime(Date date) {
		return DateTimeFormat.getFormat(TIME_SHORT_FORMAT).format(date);
	}

	/**
	 * Get a {@link Timestamp} from a date and time (String representation).
	 * 
	 * @param date the day, month and year representation
	 * @param time the time representation (e.g. 22:30)
	 * @return the {@link Timestamp} obtained from a date and time
	 */
	public static Timestamp getTimeStamp(Date date, String time) {
		DateTimeFormat dtf = DateTimeFormat.getFormat("dd.MM.yyyy HH:mm");
		Date dateTime = dtf.parse(getDay(date) + "." + getMonth(date) + "." + getYear(date) + " " + time);
		return new Timestamp(dateTime.getTime());
	}

	public static String getYear(Date date) {
		return DateTimeFormat.getFormat("yyyy").format(date);
	}

	public static String getMonth(Date date) {
		return DateTimeFormat.getFormat("MM").format(date);
	}

	public static String getDay(Date date) {
		return DateTimeFormat.getFormat("dd").format(date);
	}

	public static String getHour(Date date) {
		return DateTimeFormat.getFormat("HH").format(date);
	}

	public static String getMinute(Date date) {
		return DateTimeFormat.getFormat("mm").format(date);
	}

	public static String getSecond(Date date) {
		return DateTimeFormat.getFormat("ss").format(date);
	}

	/**
	 * Check whether 2 dates are the same day
	 * 
	 * @param dateA
	 * @param dateB
	 * @return true if it is the same day, false otherwise
	 */
	public static boolean isSameDay(Date dateA, Date dateB) {
		DateTimeFormat dtf = DateTimeFormat.getFormat("dd.MM.yyyy");
		return dtf.format(dateA).equals(dtf.format(dateB));
	}

	/**
	 * Get a {@link Timestamp} from a short date String date
	 * 
	 * @param shortDate a short date representation (e.g. '2018-11-01')
	 * @return the {@link Timestamp} representation of the short date
	 */
	public static Timestamp getDayBeforeTimestamp(String shortDate) {
		DateTimeFormat dtf = DateTimeFormat.getFormat(SHORT_DATE_FORMAT);
		Date date = dtf.parse(shortDate);
		CalendarUtil.addDaysToDate(date, -1);
		return new Timestamp(date.getTime());
	}

	/**
	 * Get a {@link Timestamp} from a short date String date
	 * 
	 * @param shortDate a short date date representation (e.g. '2018-11-01')
	 * @return the {@link Timestamp} representation of the short date date
	 */
	public static Timestamp getDateTimestamp(String shortDate) {
		DateTimeFormat dtf = DateTimeFormat.getFormat(SHORT_DATE_FORMAT);
		Date date = dtf.parse(shortDate);
		return new Timestamp(date.getTime());
	}

	/**
	 * Get a date time rounded up to 30 minutes. E.g. (13:43 -> 14:00)
	 * 
	 * @param date the date to round
	 * @return the {@link Timestamp} of a date rounded to 30 minutes later
	 */
	public static Timestamp getDateTimeRoundedTo30Min(Date date) {
		DateTimeFormat dtf = DateTimeFormat.getFormat("dd.MM.yyyy HH:mm");
		int unroundedMin = Integer.valueOf(getMinute(date));
		int mod = unroundedMin % 30;
		int time = mod == 0 ? unroundedMin : unroundedMin + 30 - mod;
		Date dateTime = dtf
				.parse(getDay(date) + "." + getMonth(date) + "." + getYear(date) + " " + getHour(date) + ":" + time);
		return new Timestamp(dateTime.getTime());
	}

	/**
	 * Get a date 1 hour later.
	 * 
	 * @param date the original date
	 * @return the date 1 hour later
	 */
	public static Timestamp get1HourLater(Date date) {
		long later = date.getTime() + 1000 * 60 * 60; // add 60 minutes
		return new Timestamp(later);
	}

	/**
	 * Get the Predefined date/time format based on the date precision.
	 * 
	 * @param enumDatePrecision the date definition precision
	 * @return the Predefined date/time format based on the date precision
	 */
	public static PredefinedFormat getFormatByPrecision(EnumDatePrecision enumDatePrecision) {
		PredefinedFormat format = PredefinedFormat.DATE_MEDIUM;
		if (EnumDatePrecision.YEAR.equals(enumDatePrecision)) {
			format = PredefinedFormat.YEAR;
		} else if (EnumDatePrecision.MONTH.equals(enumDatePrecision)) {
			format = PredefinedFormat.YEAR_MONTH;
		}
		return format;
	}
}
