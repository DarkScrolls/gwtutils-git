package com.ebalde.gwtutils.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 * Common handlers, used among multiple panels
 * 
 * @author diogo
 *
 */
public class HandlerUtils {

	/**
	 * A click handler that only stops the click event propagation from a given element from its parents.
	 */
	public static final ClickHandler STOP_EVENT_PROPRAGATION_HANDLER = new ClickHandler() {
		@Override
		public void onClick(ClickEvent event) {
			// stop the click event propagation, so that the file detail window does not open
			event.stopPropagation();
		}
	};

}
