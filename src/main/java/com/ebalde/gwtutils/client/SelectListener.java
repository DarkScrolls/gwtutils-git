package com.ebalde.gwtutils.client;

/**
 * Interface for a select listener
 */
public interface SelectListener {
	/**
	 * Called when the item is selected
	 */
	public void onSelect();
}
