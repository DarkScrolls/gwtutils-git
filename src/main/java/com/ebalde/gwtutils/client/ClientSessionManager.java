package com.ebalde.gwtutils.client;

import java.util.Date;
import java.util.logging.Logger;

import com.ebalde.gwtutils.shared.UserSession;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;

public class ClientSessionManager {

	/**
	 * Duration remembering login. Set to 1 minute - it should be less than Tomcat's 30 minute timeout, otherwise there
	 * is a long time that the client will not check the session on the server, which might have expired (e.g. server
	 * restarted, etc.).
	 */
	public static final long DURATION = 1000 * 60;

	/**
	 * Create a remote service proxy to talk to the server-side services.
	 */
	private static final UserSessionServiceAsync userSessionService = GWT.create(UserSessionService.class);

	static final Logger logger = Logger.getLogger(ClientSessionManager.class.getName());

	private static UserSession userSession = null;

	/**
	 * Creates the user session (should be called when the user logs in).
	 * 
	 * @param userSession
	 *            the user who logged in
	 */
	public static void createUserSession(UserSession userSession) {
		String sessionID = userSession.getEmail();
		Date expires = new Date(System.currentTimeMillis() + ClientSessionManager.DURATION);
		Cookies.setCookie("sid", sessionID, expires);
		ClientSessionManager.userSession = userSession;
	}

	/**
	 * Invalidate the client side session
	 */
	private static void invalidateClientSession() {
		Cookies.removeCookie("sid");
		userSession = null;
	}

	/**
	 * Invalidate the user session, both on client and server side.
	 */
	public static void logout() {
		invalidateClientSession();
		userSessionService.logout(new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				logger.info("Successfully logged out");
			}

			@Override
			public void onFailure(Throwable caught) {
				logger.severe("Logout fail");
			}
		});
	}

	/**
	 * Load the data of the logged in user, or throw an exception if there is no session.
	 * 
	 * If the value of {@link #userSession} is null (e.g. after refresh), or if the session cookie expired, the data
	 * must be fetched from the session in the server.
	 * 
	 * @param callback
	 *            to return the session user, or an exception
	 * @param forceServerStatus
	 *            set to true to force the server query, avoiding cache
	 */
	public static void getUserSession(AsyncCallback<UserSession> callback, boolean forceServerStatus) {
		if (userSession != null && Cookies.getCookie("sid") != null && !forceServerStatus) {
			callback.onSuccess(userSession);
		} else {
			userSessionService.getUserSession(new AsyncCallback<UserSession>() {

				@Override
				public void onSuccess(UserSession result) {
					if (result != null) {
						createUserSession(result);
						callback.onSuccess(result);
					} else {
						invalidateClientSession();
						InvocationException iex = new InvocationException("No valid session");
						callback.onFailure(iex);
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					invalidateClientSession();
					callback.onFailure(caught);
				}
			});
		}
	}
}
