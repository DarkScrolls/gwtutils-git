package com.ebalde.gwtutils.client;

import com.google.gwt.user.client.ui.ListBox;

public class ListBoxExtended extends ListBox {

	public String getAttId() {
		return this.getElement().getAttribute("id");
	}

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}

}
