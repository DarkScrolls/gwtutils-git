package com.ebalde.gwtutils.client;

import com.ebalde.gwtutils.shared.UserSession;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UserSessionServiceAsync {

	void login(String email, String password, AsyncCallback<UserSession> callback);

	void logout(AsyncCallback<Void> callback);

	void getUserSession(AsyncCallback<UserSession> callback);

}
