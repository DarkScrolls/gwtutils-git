package com.ebalde.gwtutils.client;

import com.google.gwt.user.client.ui.TextBox;

public class TextBoxExtended extends TextBox {

	public String getAttId() {
		return this.getElement().getAttribute("id");
	}

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}

	public void setPlaceholder(String placeholder) {
		this.getElement().setAttribute("placeholder", placeholder);
	}
	
	public void setType(String type) {
		this.getElement().setAttribute("type", type);
	}

	public void setAriaDescribedBy(String ariaDescribedBy) {
		this.getElement().setAttribute("aria-describedby", ariaDescribedBy);		
	}
}