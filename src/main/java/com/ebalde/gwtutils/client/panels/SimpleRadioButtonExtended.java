package com.ebalde.gwtutils.client.panels;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimpleRadioButton;

public class SimpleRadioButtonExtended extends SimpleRadioButton {

	@UiConstructor
	public SimpleRadioButtonExtended() {
		super(HTMLPanel.createUniqueId());
	}

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}

}
