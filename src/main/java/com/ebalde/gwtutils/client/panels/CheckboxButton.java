package com.ebalde.gwtutils.client.panels;

import java.util.logging.Logger;

import com.ebalde.gwtutils.client.ChangeListener;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;

public class CheckboxButton extends Composite {

	private static final String BTN_CHECKED = "btn btn-primary";

	private static final String BTN_UNCHECKED = "btn btn-outline-secondary";

	/**
	 * Default unchecked button icon
	 */
	private static final String ICON_UNCHECKED = "far fa-circle fa-fw";

	/**
	 * Default checked button icon
	 */
	private static final String ICON_CHECKED = "fas fa-check-circle fa-fw";

	private static CheckboxButtonUiBinder uiBinder = GWT.create(CheckboxButtonUiBinder.class);

	private boolean isChecked = false;

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@UiField
	HTMLPanel button;

	@UiField
	InlineHTML iconSpan;

	@UiField
	InlineHTML labelSpan;

	private String buttonUnchecked = BTN_UNCHECKED;

	private String buttonChecked = BTN_CHECKED;

	private String iconUnchecked = ICON_UNCHECKED;

	private String iconChecked = ICON_CHECKED;

	private ChangeListener changeListener;

	interface CheckboxButtonUiBinder extends UiBinder<Widget, CheckboxButton> {
	}

	public CheckboxButton() {
		initWidget(uiBinder.createAndBindUi(this));

		button.sinkEvents(Event.ONCLICK); // 'sink' the onCLick event
		button.addHandler(clickHandler, ClickEvent.getType()); // add the click handler to the div

		renderButton();
	}

	@UiConstructor
	public CheckboxButton(String label) {
		this();
		labelSpan.setHTML(label);
	}

	public void setStyleClassName(String styleClassName) {
		iconSpan.setStyleName(styleClassName);
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
		renderButton();
	}

	ClickHandler clickHandler = new ClickHandler() {

		@Override
		public void onClick(ClickEvent event) {
			logger.info(iconSpan.getStyleName());

			isChecked = !isChecked;
			renderButton();

			if (changeListener != null) {
				changeListener.onChange();
			}
		}
	};

	private void renderButton() {
		if (isChecked) {
			button.removeStyleName(buttonUnchecked);
			button.setStyleName(buttonChecked);
			iconSpan.removeStyleName(iconUnchecked);
			iconSpan.setStyleName(iconChecked);
		} else {
			button.removeStyleName(buttonChecked);
			button.setStyleName(buttonUnchecked);
			iconSpan.removeStyleName(iconChecked);
			iconSpan.setStyleName(iconUnchecked);
		}
	}

	public ChangeListener getChangeListener() {
		return changeListener;
	}

	public void setChangeListener(ChangeListener changeListener) {
		this.changeListener = changeListener;
	}

	public String getIconUnchecked() {
		return iconUnchecked;
	}

	public void setIconUnchecked(String iconUnchecked) {
		this.iconUnchecked = iconUnchecked;
	}

	public String getIconChecked() {
		return iconChecked;
	}

	public void setIconChecked(String iconChecked) {
		this.iconChecked = iconChecked;
	}

	public String getButtonUnchecked() {
		return buttonUnchecked;
	}

	public void setButtonUnchecked(String buttonUnchecked) {
		this.buttonUnchecked = buttonUnchecked;
	}

	public String getButtonChecked() {
		return buttonChecked;
	}

	public void setButtonChecked(String buttonChecked) {
		this.buttonChecked = buttonChecked;
	}

}
