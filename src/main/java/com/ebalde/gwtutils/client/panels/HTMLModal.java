package com.ebalde.gwtutils.client.panels;

import com.ebalde.gwtutils.client.utils.JQuery;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;

public class HTMLModal extends UIObject {

	private String id = HTMLPanel.createUniqueId();

	@UiField
	HTMLDivElement modal;

	@UiField
	HTMLDivElement modalContent;

	private static HTMLModalUiBinder uiBinder = GWT.create(HTMLModalUiBinder.class);

	interface HTMLModalUiBinder extends UiBinder<Element, HTMLModal> {
	}

	public HTMLModal() {
		setElement(uiBinder.createAndBindUi(this));
		modal.id = id;
		DomGlobal.document.body.appendChild(modal);
	}

	/**
	 * Add element to modal content.
	 * 
	 * @param element to add do the model content area
	 */
	public void setContent(HTMLElement element) {
		modalContent.innerHTML = "";
		modalContent.appendChild(element);
	}

	/**
	 * Show the modal.
	 */
	public void show() {
		JQuery.Alias.$(modal).modal("show");
	}

	public HTMLElement getElemental2() {
		return modal;
	}
}
