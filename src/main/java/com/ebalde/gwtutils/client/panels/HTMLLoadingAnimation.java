package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLElement;

public class HTMLLoadingAnimation extends UIObject {
	
	@UiField
	HTMLElement htmlElement;	

	private static HTMLLoadingAnimationUiBinder uiBinder = GWT.create(HTMLLoadingAnimationUiBinder.class);

	interface HTMLLoadingAnimationUiBinder extends UiBinder<Element, HTMLLoadingAnimation> {
	}

	public HTMLLoadingAnimation() {
		setElement(uiBinder.createAndBindUi(this));
	}

	public HTMLElement getHtmlElement() {
		return htmlElement;
	}

}
