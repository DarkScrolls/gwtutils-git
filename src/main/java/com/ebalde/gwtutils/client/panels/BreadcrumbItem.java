package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class BreadcrumbItem extends Composite {

	@UiField
	HTMLPanel breadcrumb;

	@UiField
	Anchor link;

	@UiField
	Element noLink;

	private static BreadcrumbItemUiBinder uiBinder = GWT.create(BreadcrumbItemUiBinder.class);

	private String name;

	private ClickHandler clickHandler;

	private HandlerRegistration handlerManager;

	interface BreadcrumbItemUiBinder extends UiBinder<Widget, BreadcrumbItem> {
	}

	public BreadcrumbItem(String name, ClickHandler clickHandler) {
		initWidget(uiBinder.createAndBindUi(this));

		this.name = name;
		this.clickHandler = clickHandler;

		setActive(true);
	}

	@Override
	protected void onLoad() {
		if (handlerManager != null) {
			handlerManager.removeHandler(); // otherwise, it repeats handlers each time it is loaded
		}
		handlerManager = link.addClickHandler(clickHandler);
		super.onLoad();
	}

	public void setName(String name) {
		this.name = name;
		link.setText(name);
	}

	/**
	 * Explicitly set href of the link.
	 * 
	 * @param href
	 */
	public void setHref(String href) {
		link.setHref(href);
	}

	/**
	 * Set the breadcrumb as active and, therefore, not clickable
	 */
	public void setActive(boolean active) {
		if (active) {
			breadcrumb.addStyleName("active");
			link.addStyleName("d-none"); // hide the link
			noLink.setInnerHTML(name); // show the name
			noLink.removeClassName("d-none");
		} else {
			breadcrumb.removeStyleName("active");
			link.setHTML(name);
			link.removeStyleName("d-none"); // show the link
			noLink.addClassName("d-none"); // hide the name
		}
	}

	public void setClickHandler(ClickHandler clickHandler) {
		this.clickHandler = clickHandler;
	}
}
