package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLLabelElement;

public class HTMLRadio extends UIObject {
	
	@UiField
	public HTMLElement htmlElement;
	
	@UiField
	public HTMLInputElement input;
	
	@UiField
	public HTMLLabelElement label;

	private static HTMLRadioUiBinder uiBinder = GWT.create(HTMLRadioUiBinder.class);

	interface HTMLRadioUiBinder extends UiBinder<Element, HTMLRadio> {
	}

	public HTMLRadio(String radioName, String radioDataId, String radioValue) {
		setElement(uiBinder.createAndBindUi(this));
		
		input.setAttribute("data-id", radioDataId);
		input.id = HTMLPanel.createUniqueId();
		input.name = radioName;
		label.htmlFor = input.id;
		label.innerHTML = radioValue;
	}

	public HTMLElement getElemental2() {
		return htmlElement;
	}
	
	public String getDataId() {
		return input.getAttribute("data-id");
	}

}
