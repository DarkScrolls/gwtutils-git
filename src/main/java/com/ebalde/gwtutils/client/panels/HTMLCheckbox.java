package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLLabelElement;

public class HTMLCheckbox extends UIObject {
	
	@UiField
	public HTMLElement htmlElement;
	
	@UiField
	public HTMLInputElement input;
	
	@UiField
	public HTMLLabelElement label;

	private static HTMLCheckboxUiBinder uiBinder = GWT.create(HTMLCheckboxUiBinder.class);

	interface HTMLCheckboxUiBinder extends UiBinder<Element, HTMLCheckbox> {
	}

	public HTMLCheckbox(String name, String id, String value) {
		setElement(uiBinder.createAndBindUi(this));
		
		input.setAttribute("data-id", id);
		input.id = HTMLPanel.createUniqueId();
		input.name = name;
		label.htmlFor = input.id;
		label.innerHTML = value;
	}

	public HTMLCheckbox(String id, String value) {
		setElement(uiBinder.createAndBindUi(this));
		
		input.setAttribute("data-id", id);
		input.id = HTMLPanel.createUniqueId();
		label.htmlFor = input.id;
		label.innerHTML = value;
	}

	public HTMLElement getElemental2() {
		return htmlElement;
	}
	
	public String getDataId() {
		return input.getAttribute("data-id");
	}

}
