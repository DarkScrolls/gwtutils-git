package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

public class Badge extends Composite {

	private static BadgeUiBinder uiBinder = GWT.create(BadgeUiBinder.class);

	interface BadgeUiBinder extends UiBinder<Widget, Badge> {
	}

	@UiField
	InlineLabel badge;

	/**
	 * 
	 * @param name
	 *            the text in the badge
	 * @param styleName
	 *            the style name that describes the color (e.g. primary)
	 */
	public Badge(String name, String styleName) {
		initWidget(uiBinder.createAndBindUi(this));
		badge.setText(name);
		badge.addStyleName("badge-" + styleName);
	}

}
