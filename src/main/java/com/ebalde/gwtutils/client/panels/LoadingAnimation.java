package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class LoadingAnimation extends Composite {

	private static LoadingAnimationUiBinder uiBinder = GWT.create(LoadingAnimationUiBinder.class);

	interface LoadingAnimationUiBinder extends UiBinder<Widget, LoadingAnimation> {
	}

	public LoadingAnimation() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
