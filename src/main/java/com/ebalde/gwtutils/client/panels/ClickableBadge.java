package com.ebalde.gwtutils.client.panels;

import com.ebalde.gwtutils.client.ClickListener;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLAnchorElement;

public class ClickableBadge extends UIObject {

	@UiField
	HTMLAnchorElement badge;

	private ClickListener clickListener;

	private static ClickableBadgeUiBinder uiBinder = GWT.create(ClickableBadgeUiBinder.class);

	interface ClickableBadgeUiBinder extends UiBinder<Element, ClickableBadge> {
	}

	/**
	 *
	 * @param name      the text in the badge
	 * @param styleName the style name that describes the color (e.g. primary)
	 */
	public ClickableBadge(String name, String styleName) {
		setElement(uiBinder.createAndBindUi(this));
		badge.innerHTML = name;
		badge.classList.add("text-bg-" + styleName);

		badge.addEventListener("click", evt -> {
			if (clickListener != null) {
				clickListener.onClick(name);
				evt.stopPropagation();
			}
		});
	}

	public ClickListener getClickListener() {
		return clickListener;
	}

	public void setClickListener(ClickListener clickListener) {
		this.clickListener = clickListener;
	}

	public HTMLAnchorElement getElemental2() {
		return badge;
	}

}
