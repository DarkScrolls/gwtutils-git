package com.ebalde.gwtutils.client.panels;

import com.ebalde.gwtutils.client.JsUtils;
import com.ebalde.gwtutils.shared.EnumBootstrapColor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ColorPicker extends Composite {

	/**
	 * Whether the secondary Bootstrap colors should be enabled
	 */
	private EnumBootstrapColor selectedColor;

	@UiField
	HTMLPanel buttonList;

	@UiField
	Button indigo;

	@UiField
	Button purple;

	@UiField
	Button pink;

	@UiField
	Button orange;

	@UiField
	Button teal;

	private boolean enableSecondaryColors = false;

	private static ColorPickerUiBinder uiBinder = GWT.create(ColorPickerUiBinder.class);

	interface ColorPickerUiBinder extends UiBinder<Widget, ColorPicker> {
	}

	public ColorPicker() {
		initWidget(uiBinder.createAndBindUi(this));
		toogleSecondaryColors();
	}

	@Override
	protected void onLoad() {
		loadSelection();
		super.onLoad();
	}

	private void loadSelection() {
		// firstly, hide all check icons by making the icon color the same as its background
		for (EnumBootstrapColor c : EnumBootstrapColor.values()) {
			JsUtils.addClassToElements(".colorPicker .btn.btn-" + c.getStyleClassName(),
					"text-" + c.getStyleClassName());
		}

		if (selectedColor != null) { // display the check icon on the selected button
			JsUtils.removeClassFromElements(".colorPicker .btn.btn-" + selectedColor.getStyleClassName(),
					"text-" + selectedColor.getStyleClassName());
		}
	}

	public EnumBootstrapColor getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(EnumBootstrapColor selectedColor) {
		this.selectedColor = selectedColor;
	}

	public void setEnabled(boolean enabled) {
		getElement().setPropertyBoolean("disabled", !enabled);
		for (int i = 0; i < buttonList.getWidgetCount(); i++) {
			if (enabled) {
				buttonList.getWidget(i).getElement().removeAttribute("disabled");
			} else {
				buttonList.getWidget(i).getElement().setAttribute("disabled", "disabled");
			}
		}
	}

	@UiHandler("primary")
	void onPrimaryButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.PRIMARY);
		loadSelection();
	}

	@UiHandler("secondary")
	void onSecondaryButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.SECONDARY);
		loadSelection();
	}

	@UiHandler("success")
	void onSuccessButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.SUCCESS);
		loadSelection();
	}

	@UiHandler("info")
	void onInfoButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.INFO);
		loadSelection();
	}

	@UiHandler("warning")
	void onWarningButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.WARNING);
		loadSelection();
	}

	@UiHandler("danger")
	void onDangerButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.DANGER);
		loadSelection();
	}

	@UiHandler("light")
	void onLightButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.LIGHT);
		loadSelection();
	}

	@UiHandler("dark")
	void onDarkButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.DARK);
		loadSelection();
	}

	@UiHandler("indigo")
	void onIndigoButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.INDIGO);
		loadSelection();
	}

	@UiHandler("purple")
	void onPurpleButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.PURPLE);
		loadSelection();
	}

	@UiHandler("pink")
	void onPinkButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.PINK);
		loadSelection();
	}

	@UiHandler("orange")
	void onOrangeButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.ORANGE);
		loadSelection();
	}

	@UiHandler("teal")
	void onTealButtonClick(ClickEvent event) {
		setSelectedColor(EnumBootstrapColor.TEAL);
		loadSelection();
	}

	public boolean isEnableSecondaryColors() {
		return enableSecondaryColors;
	}

	public void setEnableSecondaryColors(boolean enableSecondaryColors) {
		this.enableSecondaryColors = enableSecondaryColors;
		toogleSecondaryColors();
	}

	public void toogleSecondaryColors() {
		if (enableSecondaryColors) {
			indigo.setVisible(true);
			purple.setVisible(true);
			pink.setVisible(true);
			orange.setVisible(true);
			teal.setVisible(true);
		} else {
			indigo.setVisible(false);
			purple.setVisible(false);
			pink.setVisible(false);
			orange.setVisible(false);
			teal.setVisible(false);
		}
	}

}
