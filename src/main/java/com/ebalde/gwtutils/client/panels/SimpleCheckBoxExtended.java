package com.ebalde.gwtutils.client.panels;

import com.google.gwt.user.client.ui.SimpleCheckBox;

public class SimpleCheckBoxExtended extends SimpleCheckBox {

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}	
}
