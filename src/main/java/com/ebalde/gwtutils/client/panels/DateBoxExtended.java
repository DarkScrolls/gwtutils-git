package com.ebalde.gwtutils.client.panels;

import java.util.ArrayList;
import java.util.Date;

import com.ebalde.gwtutils.client.TextBoxExtended;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsDate;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Date input which includes a date picker when the user focus on it. Based on jQuery UI DatePicker. Made to work inside
 * Modal / DialogBox, etc.
 * 
 * @author diogo
 *
 */
public class DateBoxExtended extends Composite {

	private Date date = null;

	/**
	 * The date formats related to the widget, including the display date format, the jQuery format to parse dates and
	 * the equivalent Java format.
	 * 
	 * @author diogo
	 *
	 */
	static class DateFormat {
		private String displayFormat;
		private String jqueryFormat;
		private String javaFormat;

		public DateFormat(String display, String jquery, String java) {
			super();
			/**
			 * The widget display date format
			 */
			this.displayFormat = display;
			/**
			 * The jQuery date format for processing dates
			 */
			this.jqueryFormat = jquery;
			/**
			 * The Java date format equivalent to the one used for jQuery to process dates
			 */
			this.javaFormat = java;
		}

		public String getDisplayFormat() {
			return displayFormat;
		}

		public void setDisplayFormat(String displayFormat) {
			this.displayFormat = displayFormat;
		}

		public String getJqueryFormat() {
			return jqueryFormat;
		}

		public void setJqueryFormat(String jqueryFormat) {
			this.jqueryFormat = jqueryFormat;
		}

		public String getJavaFormat() {
			return javaFormat;
		}

		public void setJavaFormat(String javaFormat) {
			this.javaFormat = javaFormat;
		}

	}

	private static final DateFormat DATEFORMAT = new DateFormat("d M yy", "dd.mm.yy", "dd.MM.yyyy");

	@UiField
	TextBoxExtended dateInput;

	String appendToSelector;

	private static DateBoxExtendedUiBinder uiBinder = GWT.create(DateBoxExtendedUiBinder.class);

	private String minDate;

	private String maxDate;

	private JsArrayInteger weekDays = JsArrayInteger.createArray().cast();

	interface DateBoxExtendedUiBinder extends UiBinder<Widget, DateBoxExtended> {
	}

	public DateBoxExtended() {
		initWidget(uiBinder.createAndBindUi(this));
		dateInput.setAttId(HTMLPanel.createUniqueId());
	}

	/**
	 * Append DatePicker to a specific HTML element. Useful to make it work with dialog, popup, etc.
	 * 
	 * @param selector
	 *            the selector to append the DatePicker to (e.g. '.dialogContent')
	 */
	public void setAppendTo(String selector) {
		appendToSelector = selector;
	}

	@Override
	protected void onLoad() {
		if (appendToSelector == null) {
			initialiseWidget(dateInput.getAttId());
		} else {
			initialiseWidgetOnDialog(dateInput.getAttId(), appendToSelector);
		}
		setJSDateTimeFormat(dateInput.getAttId(), DATEFORMAT.getDisplayFormat());
		if (date != null) {
			setDate(dateInput.getAttId(), JsDate.create((double) date.getTime()));
		}
		
		if (minDate != null) {
			setMinDate(dateInput.getAttId(), minDate);
		}
		
		if (maxDate != null) {
			setMaxDate(dateInput.getAttId(), maxDate);
		}
		if (weekDays != null && weekDays.length() > 0) {
			setSelectableWeekdays(dateInput.getAttId(), weekDays);
		}
		
		super.onLoad();
	}

	public String getAttId() {
		return this.getElement().getAttribute("id");
	}

	public void setAttId(String id) {
		this.getElement().setAttribute("id", id);
	}

	public native void initialiseWidget(String inputId) /*-{
		$wnd.jQuery('#' + inputId).datepicker({
			changeMonth : true,
			changeYear : true
		});
	}-*/;

	public native void setJSDateTimeFormat(String inputId, String dateTimeFormat) /*-{
		$wnd.jQuery('#' + inputId).datepicker("option", "dateFormat", dateTimeFormat);
	}-*/;

	public native void setDate(String inputId, JsDate date) /*-{
		$wnd.jQuery('#' + inputId).datepicker("setDate", date);
	}-*/;

	public native String getDatePickerDate(String inputId, String datetimeFormat) /*-{
		// whatever dateFormat give, it always returns the dateTimeDisplayFormat set before
		return $wnd.jQuery.datepicker.formatDate(datetimeFormat, $wnd.jQuery('#' + inputId).datepicker("getDate"));
	}-*/;

	public native void initialiseWidgetOnDialog(String inputId, String selector) /*-{
		$wnd.jQuery('#' + inputId).datepicker({
			beforeShow : function(textbox, instance) {
				setTimeout(function() {
					$wnd.jQuery("#ui-datepicker-div").position({
						my : "left top",
						at : "left bottom",
						of : "#" + inputId
					});
				}, 1);
			},
			changeMonth : true,
			changeYear : true
		});

		// fix bug which makes modal close on focus on widgets outside of it
		$wnd.jQuery("#ui-datepicker-div").detach().appendTo(selector);
	}-*/;

	/**
	 * Set the minimum selectable date.
	 * 
	 * @param inputId the Id of the input
	 * @param value minimum selectable date as actual date (new Date(2009, 1 - 1, 26)), as a numeric offset from today
	 *              (-20), or as a string of periods and units ('+1M +10D'). For the last, use 'D' for days, 'W' for
	 *              weeks, 'M' for months, or 'Y' for years.
	 */
	public native void setMinDate(String inputId, String value) /*-{
		$wnd.jQuery('#' + inputId).datepicker("option", "minDate", value);
	}-*/;

	/**
	 * Set the maximum selectable date.
	 * 
	 * @param inputId the Id of the input
	 * @param value maximum selectable date as actual date (new Date(2009, 1 - 1, 26)), as a numeric offset from today
	 *              (-20), or as a string of periods and units ('+1M +10D'). For the last, use 'D' for days, 'W' for
	 *              weeks, 'M' for months, or 'Y' for years.
	 */
	public native void setMaxDate(String inputId, String value) /*-{
		$wnd.jQuery('#' + inputId).datepicker("option", "maxDate", value);
	}-*/;

	/**
	 * Set selectable week days.
	 * 
	 * @param inputId the Id of the input
	 * @param weekDays the selectable week days (numbers from 0 to 6).
	 */
	public native void setSelectableWeekdays(String inputId, JsArrayInteger weekDays) /*-{
		$wnd.jQuery('#' + inputId).datepicker("option", "beforeShowDay", function(date){
               var day = date.getDay(); 
               return [weekDays.includes(day),""];
      });
	}-*/;

	/**
	 * Get the input date, or null if empty
	 * 
	 * @return the input date, or null if empty
	 */
	public Date getDate() {
		Date date = null;
		String datepickerDate = getDatePickerDate(dateInput.getAttId(), DATEFORMAT.getJqueryFormat());
		if (datepickerDate != null && !datepickerDate.isEmpty()) {
			DateTimeFormat dtf = DateTimeFormat.getFormat(DATEFORMAT.getJavaFormat());
			date = dtf.parse(datepickerDate);
		}
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
		if (date != null) {
			setDate(dateInput.getAttId(), JsDate.create((double) date.getTime()));
		}
	}

	public String getDateTimeFormat() {
		return DATEFORMAT.getDisplayFormat();
	}

	public void setDateTimeFormat(String dateTimeFormat) {
		DATEFORMAT.setDisplayFormat(dateTimeFormat);
	}

	public TextBoxExtended getDateInput() {
		return dateInput;
	}
	
	public void setMinDate(String value) {
		this.minDate = value;
	}
	
	public void setMaxDate(String value) {
		this.maxDate = value;
	}
	
	/**
	 * Set selectable week days (numbers from 0 to 6). To be used in uibinder.
	 * 
	 * @param weekDaysStr the list of week days to set as selectable e.g. "1,4,5"
	 */
	public void setSelectableWeekdays(String weekDaysStr) {
		String[] wd = weekDaysStr.split(",");
		for (int i = 0; i < wd.length; i++) {
			weekDays.push(Integer.valueOf(wd[i]));
		}
	}
	
	/**
	 * Set selectable week days (numbers from 0 to 6). To set programatically.
	 * 
	 * @param weekDays the list of week days to set as selectable e.g. "1,4,5"
	 */
	public void setSelectableWeekdays(ArrayList<Integer> weekDayList) {
		for (Integer i : weekDayList) {
			weekDays.push(i);
		}
	}

}
