package com.ebalde.gwtutils.client.panels;

import java.sql.Date;

import com.ebalde.gwtutils.client.InputValidator;
import com.ebalde.gwtutils.client.utils.FormInputUtils;
import com.ebalde.gwtutils.client.utils.HTMLElementUtils;
import com.ebalde.gwtutils.client.utils.StringUtils;
import com.ebalde.gwtutils.shared.DateVariablePrecision;
import com.ebalde.gwtutils.shared.EnumDatePrecision;
import com.ebalde.gwtutils.shared.FieldVerifier;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;

public class HTMLDateInput3Fields extends UIObject {

	@UiField
	HTMLElement htmlElement;

	@UiField
	HTMLInputElement day;

	@UiField
	HTMLInputElement month;

	@UiField
	HTMLInputElement year;

	@UiField
	HTMLElement sep1;

	@UiField
	HTMLElement sep2;

	@UiField
	HTMLElement invalidFeedback;

	private static HTMLDateInput3FieldsUiBinder uiBinder = GWT.create(HTMLDateInput3FieldsUiBinder.class);

	interface HTMLDateInput3FieldsUiBinder extends UiBinder<Element, HTMLDateInput3Fields> {
	}

	public HTMLDateInput3Fields() {
		setElement(uiBinder.createAndBindUi(this));

		FormInputUtils.setUniqueId(day);
		FormInputUtils.setUniqueId(month);
		FormInputUtils.setUniqueId(year);

		// jump to next date field when input is complete
		day.addEventListener("keyup", evt -> {
			if (day.value.matches("^\\d{2}$")) {
				month.focus();
			}
		});
		month.addEventListener("keyup", evt -> {
			if (month.value.matches("^\\d{2}$")) {
				year.focus();
			}
		});
	}

	/**
	 * Set the date values of a date separated into day, month and year fields, based on {@link EnumDatePrecision}.
	 * 
	 * @param date              the date to set
	 * @param enumDatePrecision the precision with which the date was saved
	 * @param day               the day HTML input element
	 * @param month             the month HTML input element
	 * @param year              the year HTML input element
	 */
	public void setDate(Date date, EnumDatePrecision enumDatePrecision) {
		if (date != null) {
			if (EnumDatePrecision.YEAR.equals(enumDatePrecision)) {
				year.value = StringUtils.toString(date, PredefinedFormat.YEAR);
			} else if (EnumDatePrecision.MONTH.equals(enumDatePrecision)) {
				year.value = StringUtils.toString(date, PredefinedFormat.YEAR);
				month.value = date != null ? DateTimeFormat.getFormat("MM").format(date) : "";
			} else {
				year.value = StringUtils.toString(date, PredefinedFormat.YEAR);
				month.value = date != null ? DateTimeFormat.getFormat("MM").format(date) : "";
				day.value = date != null ? DateTimeFormat.getFormat("dd").format(date) : "";
			}
		}
	}

	public DateVariablePrecision getDateVariablePrecision() {
		return HTMLElementUtils.toDateVariablePrecision(day, month, year);
	}

	public void assertValidDate() {
	    boolean isDayEmpty = day.value.isEmpty();
	    boolean isMonthEmpty = month.value.isEmpty();
	    boolean isYearEmpty = year.value.isEmpty();

	    boolean isValid;

	    if (isYearEmpty && isMonthEmpty && isDayEmpty) {
	        // All fields are empty, which is a valid state
	        isValid = true;
	    } else if (!isYearEmpty && isMonthEmpty && isDayEmpty) {
	        // Only year is filled
	        isValid = FieldVerifier.isValidDate(year.value, DateTimeFormat.getFormat(PredefinedFormat.YEAR).getPattern());
	    } else if (!isYearEmpty && !isMonthEmpty && isDayEmpty) {
	        // Year and month are filled
	        String yearMonthString = month.value + "/" + year.value;
	        isValid = FieldVerifier.isValidDate(yearMonthString, DateTimeFormat.getFormat("MM/yyyy").getPattern());
	    } else if (!isYearEmpty && !isMonthEmpty && !isDayEmpty) {
	        // All fields are filled
	        String dateString = day.value + "/" + month.value + "/" + year.value;
	        isValid = FieldVerifier.isValidDate(dateString, DateTimeFormat.getFormat("dd/MM/yyyy").getPattern());
	    } else {
	        // Any other combination of filled/empty fields is invalid
	        isValid = false;
	    }

	    InputValidator.assertInputValid(day, isValid);
	    InputValidator.assertInputValid(month, isValid);
	    InputValidator.assertInputValid(year, isValid);

	    if (!isValid) {
	        invalidFeedback.classList.add("d-block");
	    }
	}

	public void setPlaceholders(String dd, String mm, String yyyy) {
		day.placeholder = dd;
		month.placeholder = mm;
		year.placeholder = yyyy;
	}

	public void setSeparator(String separator) {
		sep1.innerHTML = separator;
		sep2.innerHTML = separator;
	}

	public void setInvalidDateMsg(String msg) {
		invalidFeedback.innerHTML = msg;
	}

	public HTMLElement getElemental2() {
		return htmlElement;
	}

}
