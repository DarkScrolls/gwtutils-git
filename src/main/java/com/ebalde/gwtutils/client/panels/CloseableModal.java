package com.ebalde.gwtutils.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class CloseableModal extends Composite {

	private String id = HTMLPanel.createUniqueId();

	@UiField
	HTMLPanel modalContent;

	private static CloseableModalUiBinder uiBinder = GWT.create(CloseableModalUiBinder.class);

	interface CloseableModalUiBinder extends UiBinder<Widget, CloseableModal> {
	}

	public CloseableModal() {
		initWidget(uiBinder.createAndBindUi(this));
		this.getElement().setId(id);
		RootPanel.get().add(this);
	}

	public static native void openModal(String id) /*-{
		$wnd.jQuery('#' + id).modal('show');
	}-*/;

	/**
	 * Set the HTML content of the modal
	 * 
	 * @param html
	 *            the HTML content to set to the modal
	 */
	public void setHtmlContent(String html) {
		setContent(new HTMLPanel(html));
	}

	/**
	 * Add widget to modal (before and after the close buttons).
	 * 
	 * @param widget
	 *            to add do the model content area
	 */
	public void setContent(Widget widget) {
		modalContent.clear();
		modalContent.add(widget);
	}

	/**
	 * Show this modal
	 */
	public void show() {
		openModal(id);
	}

}
