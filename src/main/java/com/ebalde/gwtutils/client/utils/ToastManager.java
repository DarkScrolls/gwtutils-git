package com.ebalde.gwtutils.client.utils;

import com.google.gwt.user.client.ui.RootPanel;

public class ToastManager {
	public static ToastPanel toastPanel = new ToastPanel();

	public ToastManager() {
		RootPanel.get().add(toastPanel);
	}

	public void showToast(Toast toast) {
		toastPanel.toastContainer.appendChild(toast.getToastElemental2());
		JQuery.Alias.$("#"+toast.getId()).toast("show");
	}

	public void showToast(String text, String stylename) {
		showToast(new Toast(text, stylename));
	}
}
