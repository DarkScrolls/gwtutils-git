package com.ebalde.gwtutils.client.utils.autocomplete;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AutoCompleteListener<T> {
	
	public void getSearchSuggestions(String search, AsyncCallback<ArrayList<T>> asyncCallback);
}