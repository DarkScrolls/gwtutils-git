package com.ebalde.gwtutils.client.utils;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = JsPackage.GLOBAL, isNative = true, name = "Object")
public class SelectizeOption {

	@JsProperty
	public String value;

	@JsProperty
	public String text;
	
	@JsProperty
	public boolean selected;
	
	// to include badges in the selectize options
	@JsProperty
	public String badge1;
	
	@JsProperty
	public String badge2;
}