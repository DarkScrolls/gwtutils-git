package com.ebalde.gwtutils.client.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.HTMLPanel;

import elemental2.dom.Element;

/**
 * Tooltip manager for Bootstrap 5
 * 
 * @author diogo
 *
 */
public class TooltipManager {
	private static List<Tooltip> tooltips = new ArrayList<>();

	/**
	 * Adds a tooltip to a specified target element within a container. The tooltip will be positioned relative to the
	 * container, and the container must have a unique ID. If the container does not have an ID, one will be generated.
	 *
	 * @param text      The text to be displayed inside the tooltip.
	 * @param target    The target element to which the tooltip will be attached.
	 * @param container The container element within which the tooltip will be positioned. The container must have a
	 *                  unique ID, or one will be generated.
	 * @return the Tooltip JS native object
	 */
	public static void addTooltip(String text, Element target, Element container) {
		addTooltip(text, target, container, "auto");
	}

	/**
	 * Adds a tooltip to a specified target element within a container. The tooltip will be positioned relative to the
	 * container, and the container must have a unique ID. If the container does not have an ID, one will be generated.
	 *
	 * @param text      The text to be displayed inside the tooltip.
	 * @param target    The target element to which the tooltip will be attached.
	 * @param container The container element within which the tooltip will be positioned. The container must have a
	 *                  unique ID, or one will be generated.
	 * @param position  The position of the tooltip relative to the target element. Acceptable values may include "top",
	 *                  "bottom", "left", "right", etc.
	 */
	public static void addTooltip(String text, Element target, Element container, String position) {
		Scheduler.get().scheduleDeferred(() -> {
			Tooltip.Options options = new Tooltip.Options();
			options.placement = position;
			options.title = text;
			options.html = true;
			if (container.id.isEmpty()) {
				container.id = HTMLPanel.createUniqueId();
			}
			options.container = "#" + container.id;

			Tooltip tooltip = new Tooltip(target, options);
			tooltips.add(tooltip);
		});
	}

	/**
	 * Adds a tooltip to a specified target element within a container. The tooltip will be positioned relative to the
	 * container, and the container must have a unique ID. If the container does not have an ID, one will be generated.
	 * This method will try to create the tooltip immediately, does not use Scheduler.get().scheduleDeferred(), so this
	 * method should be used with caution: the container must be already attached to the DOM.
	 *
	 * @param text      The text to be displayed inside the tooltip.
	 * @param target    The target element to which the tooltip will be attached.
	 * @param container The container element within which the tooltip will be positioned. The container must have a
	 *                  unique ID, or one will be generated.
	 * @return the Tooltip JS native object
	 */
	public static Tooltip addImmediateTooltip(String text, Element target, Element container) {
		return addImmediateTooltip(text, target, container, "auto");
	}

	/**
	 * Adds a tooltip to a specified target element within a container. The tooltip will be positioned relative to the
	 * container, and the container must have a unique ID. If the container does not have an ID, one will be generated.
	 * This method will try to create the tooltip immediately, does not use Scheduler.get().scheduleDeferred(), so this
	 * method should be used with caution: the container must be already attached to the DOM.
	 *
	 * @param text     The text to be displayed inside the tooltip.
	 * @param target   The target element to which the tooltip will be attached.
	 * @param position The position of the tooltip relative to the target element. Acceptable values may include "top",
	 *                 "bottom", "left", "right", etc.
	 * @return the Tooltip JS native object
	 */
	public static Tooltip addImmediateTooltip(String text, Element target, Element container, String position) {
		if (target == null || container == null) {
			return null;
		}
		Tooltip.Options options = new Tooltip.Options();
		options.placement = position;
		options.title = text;
		options.html = true;
		if (container.id.isEmpty()) {
			container.id = HTMLPanel.createUniqueId();
		}
		options.container = "#" + container.id;
		Tooltip tooltip = new Tooltip(target, options);
		tooltips.add(tooltip);
		return tooltip;
	}

	/**
	 * Disposes all tooltips on the page.
	 */
	public static void disposeAllTooltips() {
		// Dispose each tooltip in the list
		for (Tooltip tooltip : tooltips) {
			tooltip.dispose();
		}
		// Clear the list after disposing
		tooltips.clear();
	}
}
