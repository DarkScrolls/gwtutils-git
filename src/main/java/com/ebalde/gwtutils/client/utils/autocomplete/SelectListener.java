package com.ebalde.gwtutils.client.utils.autocomplete;

public interface SelectListener<T> {
	public void onSelect(T selected);
}
