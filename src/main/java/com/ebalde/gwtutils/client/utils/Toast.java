package com.ebalde.gwtutils.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLImageElement;

public class Toast extends UIObject {
	
	@UiField
	HTMLDivElement toast;
	
	@UiField
	HTMLDivElement toastBody;
	
	@UiField
	public HTMLImageElement image;
	
	@UiField
	public HTMLElement title;
	
	@UiField
	public HTMLElement time;
	
	private String id;

	private static ToastUiBinder uiBinder = GWT.create(ToastUiBinder.class);

	interface ToastUiBinder extends UiBinder<Element, Toast> {
	}

	public Toast(String text, String stylename) {
		setElement(uiBinder.createAndBindUi(this));
		id = HTMLPanel.createUniqueId();
		toast.setAttribute("id", id);
		toastBody.textContent = text;
		toastBody.classList.add("text-" + stylename);
	}

	public HTMLDivElement getToastElemental2() {
		return toast;
	}

	public String getId() {
		return id;
	}

}
