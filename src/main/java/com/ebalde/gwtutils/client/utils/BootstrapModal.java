package com.ebalde.gwtutils.client.utils;

import elemental2.dom.Element;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "bootstrap.Modal")
public class BootstrapModal {

	public BootstrapModal(Element element, Options options) {
	}

	public native void show();

	public native void hide();

	public native void dispose();

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	public static class Options {
		public Object backdrop; // Can be boolean or string "static"
		public boolean keyboard;
	}
}
