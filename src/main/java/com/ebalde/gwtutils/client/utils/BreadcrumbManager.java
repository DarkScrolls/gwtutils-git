package com.ebalde.gwtutils.client.utils;

import java.util.ArrayList;

import com.ebalde.gwtutils.client.panels.BreadcrumbItem;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTMLPanel;

public class BreadcrumbManager {

	private static final String SEARCH_ICON = "<i class=\"fas fa-search me-1\"></i>";

	private static BreadcrumbItem getBreadcrumbTrash(String trashLocalizedName) {
		return new BreadcrumbItem("<i class=\"fa fa-fw fa-trash\" aria-hidden=\"true\"></i> " + trashLocalizedName,
				new ClickHandler() {
					@Override
					public void onClick(final ClickEvent event) {
					}
				});
	}

	/**
	 * Load the Breadcrumb based on the search input.
	 * 
	 * @param breadcrumb
	 *            the {@link HTMLPanel} of the Breadcrumb
	 * @param basePageBreadcrumbItem
	 *            the base item of the page (e.g. Documents, Contacts, Markers, etc.)
	 * @param searchText
	 *            the content of the search input
	 * @param isTrash
	 *            weather the trash page is opened
	 * @param trashLocalizedName
	 *            the localized name to show, e.g. 'Trash'
	 */
	public static void loadBreadCrumb(HTMLPanel breadcrumb, BreadcrumbItem basePageBreadcrumbItem, String searchText,
			boolean isTrash, String trashLocalizedName) {
		breadcrumb.clear();
		basePageBreadcrumbItem.removeFromParent();

		// base breadcrumb item
		if (isTrash || searchText != null && !searchText.isEmpty()) {
			basePageBreadcrumbItem.setActive(false);
		} else {
			basePageBreadcrumbItem.setActive(true);
		}
		breadcrumb.add(basePageBreadcrumbItem);

		// trash bin?
		if (isTrash) {
			BreadcrumbItem breadcrumbTrash = getBreadcrumbTrash(trashLocalizedName);
			breadcrumbTrash.setActive(true);
			breadcrumb.add(breadcrumbTrash);
		}

		// search breadcrumb
		if (searchText != null && !searchText.isEmpty()) {
			BreadcrumbItem searchBreadcrumbItem = new BreadcrumbItem(SEARCH_ICON + searchText, new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) { // nothing to do in this case...
				}
			});
			searchBreadcrumbItem.setActive(true);
			breadcrumb.add(searchBreadcrumbItem);
		}
	}

	/**
	 * Load the Breadcrumb based on the search input.
	 * 
	 * @param breadcrumb
	 *            the {@link HTMLPanel} of the Breadcrumb
	 * @param bcItems
	 *            the list of base breadcrumbs to add (e.g. Books / Collection, Contacts, Markers, etc.)
	 * @param searchText
	 *            the content of the search input
	 * @param isTrash
	 *            weather the trash page is opened
	 * @param trashLocalizedName
	 *            the localized name to show, e.g. 'Trash'
	 */
	public static void loadBreadCrumb(HTMLPanel breadcrumb, ArrayList<BreadcrumbItem> bcItems, String searchText,
			boolean isTrash, String trashLocalizedName) {
		breadcrumb.clear();
		for (BreadcrumbItem bc : bcItems) {
			bc.removeFromParent();
		}

		// add breadcrumbs
		for (BreadcrumbItem bc : bcItems) {
			bc.setActive(false);
			breadcrumb.add(bc);
		}
		// set last breadcrumb active if we are not checking the trash and if there is no active search
		if (!isTrash && bcItems != null && !bcItems.isEmpty() && (searchText == null || searchText.isEmpty())) {
			bcItems.get(bcItems.size() - 1).setActive(true);
		}

		// trash bin?
		if (isTrash) {
			BreadcrumbItem breadcrumbTrash = getBreadcrumbTrash(trashLocalizedName);
			breadcrumbTrash.setActive(true);
			breadcrumb.add(breadcrumbTrash);
		}

		// search breadcrumb
		if (searchText != null && !searchText.isEmpty()) {
			BreadcrumbItem searchBreadcrumbItem = new BreadcrumbItem(SEARCH_ICON + searchText, new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) { // nothing to do in this case...
				}
			});
			searchBreadcrumbItem.setActive(true);
			breadcrumb.add(searchBreadcrumbItem);
		}
	}

}
