package com.ebalde.gwtutils.client.utils;

import elemental2.dom.Element;
import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * Created by bduisenov on 27/10/15.
 */
@JsType(namespace = JsPackage.GLOBAL, name = "jQuery", isNative = true)
public class JQuery {

	@FunctionalInterface
	@JsFunction
	public interface func {
		void apply();
	}

	@FunctionalInterface
	@JsFunction
	public interface func1 {
		void apply(String val);
	}

	public native void click(func func);

	public native JQuery keypress(func func);

	public native JQuery keydown(func func);

	public native JQuery keyup(func func);

	public native JQuery focus();

	public native void change(func func);

	public native JQuery on(String action, func func);

	public native JQuery filter(String predicate);

	public native String val();

	public native void val(String val);

	public native String text();

	public native void text(String text);

	public native JQuery append(String html);

	public native JQuery append(JQuery jq);

	public native JQuery before(Element element);

	public native JQuery before(JQuery jq);

	public native JQuery children();

	public native JQuery first();

	public native JQuery append(Element jq);

	public native JQuery remove();

	public native JQuery hide();

	public native JQuery hide(int duration);

	public native JQuery hide(int duration, func complete);

	public native JQuery show();

	public native JQuery show(int duration);

	public native JQuery show(int duration, func complete);

	public native boolean is(String selector);

	public native String css(String propertyName);

	public native String css(String propertyName, String val);

	public native JQuery addClass(String val);

	public native JQuery removeClass(String val);

	public native JQuery toggleClass(String val);

	public native JQuery toggleClass(String val, boolean state);

	public native boolean hasClass(String val);

	public native String attr(String name);

	public native JQuery attr(String name, String val);

	public native JQuery fadeIn(int duration);

	public native JQuery fadeIn(int duration, func onComplete);

	public native JQuery fadeOut(int duration);

	public native JQuery fadeOut(int duration, func onComplete);

	public native JQuery width(String val);

	public native JQuery width(int val);

	public native int width();

	public native JQuery find(String selector);

	public native String html();

	public native JQuery html(String html);

	public native JQuery scrollTop(int position);

	/*
	 * It belongs to bootstrap, but was added here as it is a jQuery extension
	 */
	public native void toast(String option);

	public native void modal(String option);

	public native void modal(Object options);

	/*
	 * From Jquery UI
	 */
	public native JQuery datepicker();

	public native JQuery datepicker(String option);

	public native JQuery datepicker(String option, String value);

	public native JQuery datepicker(String option, String optionName, String value);

	public native JQuery datepicker(String option, String optionName, func func);

	/*
	 * Selectize.dev
	 */
	public native JQuery selectize();

	// General jQuery data method (for both normal data and selectize)
    public native SelectizeInstance data(String key);  // Generic method for data retrieval

	public native JQuery selectize(SelectizeOptions options);

	// Define a class for selectize instance handling
	@JsType(isNative = true, namespace = JsPackage.GLOBAL)
	public static class SelectizeInstance {
	    public native void addItem(String value);
	    public native void addOption(SelectizeOption option);
	    public native void clearOptions();
	    public native void setValue(String value);
	    public native void removeOption(String value);
	    public native void on(String event, JQuery.func callback);  // For event binding
	    public native void disable();
	    public native void enable();
	}

	@JsType(namespace = "jQuery", name = "Deferred", isNative = true)
	public static class Deferred {
		public native Deferred done(func func);

		public native Deferred fail(func func);

		public native Deferred reject();

		public native Deferred then(func doneCallbacks, func failCallbacks);

		public native String state();

		public native Deferred always(func func);

		public native Deferred resolve(String val);

		public native Deferred resolve();
	}

	public static class Alias {

		@JsMethod(namespace = JsPackage.GLOBAL, name = "jQuery")
		public static native JQuery $(String selector);

		@JsMethod(namespace = JsPackage.GLOBAL, name = "jQuery")
		public static native JQuery $(JQuery element);

		@JsMethod(namespace = JsPackage.GLOBAL, name = "jQuery")
		public static native JQuery $(Element element);

		@JsMethod(namespace = JsPackage.GLOBAL, name = "jQuery")
		public static native JQuery $(String selector, JQuery context);

		@JsMethod(namespace = JsPackage.GLOBAL, name = "jQuery")
		public static native JQuery $(String selector, Element context);
	}

}
