package com.ebalde.gwtutils.client.utils;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = JsPackage.GLOBAL, isNative = true, name = "Object")
public class SelectizeCreateParams {
	@JsProperty
	public String value;

	@JsProperty
	public String text;
}