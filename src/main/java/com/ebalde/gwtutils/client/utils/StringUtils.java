package com.ebalde.gwtutils.client.utils;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;

public class StringUtils {

	/**
	 * Cut off a string.
	 * 
	 * @param string      the string to cut
	 * @param lengthLimit the maximum length the string must have
	 * @return the cut off string, with "..." appended in case the string is longer than the character limit
	 */
	public static String cutOffString(String string, int lengthLimit) {
		String niceCutStr = string;
		if (string.length() > lengthLimit) {
			String hardCutStr = string.substring(0, lengthLimit);
			int lastWhiteSpaceIndex = getLastWhiteSpace(hardCutStr);
			niceCutStr = string.substring(0, lastWhiteSpaceIndex);
			if (lastWhiteSpaceIndex < string.length()) {
				niceCutStr += "...";
			}
		}
		return niceCutStr;
	}

	/**
	 * Get the last white space in a string
	 * 
	 * @param string the string to check
	 * @return the index of the last white space in the string
	 */
	public static int getLastWhiteSpace(String string) {
		int index = string.length();
		RegExp regExp = RegExp.compile(".*(\\s)");
		MatchResult matcher = regExp.exec(string);
		boolean matchFound = matcher != null; // equivalent to regExp.test(inputStr);
		if (matchFound) {
			index = matcher.getGroup(0).length();
		}
		return index;
	}

	public static String escapeHtml(String html) {
		String noHtml = html.toString().replaceAll("\\<.*?>", "");
		return noHtml;
	}

	/**
	 * Convert an Integer to String. Returns an empty string if Integer is null.
	 * 
	 * @param integer the Integer to convert
	 * @return the Integer String value, or an empty string if null
	 */
	public static String toString(Integer integer) {
		return toString(integer, "");
	}

	/**
	 * Convert an Integer to String. Returns the given default value if Integer is null.
	 * 
	 * @param integer      the Integer to convert
	 * @param defaultValue the default value, in case the Integer is null
	 * @return the Integer String value, or defaultValue if null
	 */
	public static String toString(Integer integer, String defaultValue) {
		return integer != null ? String.valueOf(integer) : defaultValue;
	}
	
	/**
	 * Convert a Long to String. Returns an empty string if Long is null.
	 * 
	 * @param value the Long to convert
	 * @return the Long String value, or an empty string if null
	 */
	public static String toString(Long value) {
		return toString(value, "");
	}
	
	/**
	 * Convert a Long to String. Returns the given default value if Long is null.
	 * 
	 * @param value the Long to convert
	 * @return the Long String value, or an empty string if null
	 */
	public static String toString(Long value, String defaultValue) {
		return value != null ? String.valueOf(value) : defaultValue;
	}

	/**
	 * Convert a Float to String. Returns an empty string if Float is null.
	 * 
	 * @param value the Float to convert
	 * @return the Float String value, or an empty string if null
	 */
	public static String toString(Float value) {
		return toString(value, "");
	}

	/**
	 * Convert a Float to String. Returns the given default value if Float is null.
	 * 
	 * @param value        the Float to convert
	 * @param defaultValue the default value, in case the Float is null
	 * @return the Float String value, or defaultValue if null
	 */
	public static String toString(Float value, String defaultValue) {
		return value != null ? String.valueOf(value) : defaultValue;
	}

	/**
	 * Convert a BigDecimal to String. Returns an empty string if BigDecimal is null.
	 * 
	 * @param value the BigDecimal to convert
	 * @return the BigDecimal String value, or an empty string if null
	 */
	public static String toString(BigDecimal value) {
		return toString(value, "");
	}

	/**
	 * Convert a BigDecimal to String. Returns the given default value if BigDecimal is null.
	 * 
	 * @param value        the BigDecimal to convert
	 * @param defaultValue the default value, in case the BigDecimal is null
	 * @return the BigDecimal String value, or defaultValue if null
	 */
	public static String toString(BigDecimal value, String defaultValue) {
		// Strip only values with decimal digits
		if (value != null) {
			BigDecimal striped = (value.scale() > 0) ? value.stripTrailingZeros() : value;
			// Unscale only values with ten exponent
			striped = (striped.scale() < 0) ? striped.setScale(0) : striped;
			return striped.toString();
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convert a Date to String. Returns an empty string if date is null.
	 * 
	 * @param date             the date to convert to string
	 * @param predefinedFormat the date format
	 * @return the date string, or an empty string if date is null
	 */
	public static String toString(Date date, PredefinedFormat predefinedFormat) {
		return date != null ? DateTimeFormat.getFormat(predefinedFormat).format(date) : "";
	}

	/**
	 * Convert a Date to String. Returns the given default value if date is null.
	 * 
	 * @param date             the date to convert to string
	 * @param predefinedFormat the date format
	 * @param defaultValue the value to return in case the date is null
	 * @return the date string, or the given default value if date is null
	 */
	public static String toString(Date date, PredefinedFormat predefinedFormat, String defaultValue) {
		return date != null ? DateTimeFormat.getFormat(predefinedFormat).format(date) : defaultValue;
	}

	/**
	 * Convert a Timestamp to String. Returns an empty string if Timestamp is null.
	 * 
	 * @param timestamp      the timestamp to convert to string
	 * @param dateTimeFormat the datetime format
	 * @return the date string, or an empty string if timestamp is null
	 */
	public static String toString(Timestamp timestamp, String dateTimeFormat) {
		return timestamp != null ? DateTimeFormat.getFormat(dateTimeFormat).format(timestamp) : "";
	}

	/**
	 * Convert a Timestamp to String. Returns an empty string if Timestamp is null.
	 * 
	 * @param timestamp             the timestamp to convert to string
	 * @param predefinedFormat the date format
	 * @return the date string, or an empty string if date is null
	 */
	public static String toString(Timestamp timestamp, PredefinedFormat predefinedFormat) {
		return timestamp != null ? DateTimeFormat.getFormat(predefinedFormat).format(timestamp) : "";
	}

	/**
	 * Convert a Timestamp to String. Returns the given default value if Timestamp is null.
	 * 
	 * @param timestamp      the timestamp to convert to string
	 * @param predefinedFormat the date format
	 * @return the date string, or an empty string if timestamp is null
	 */
	public static String toString(Timestamp timestamp, PredefinedFormat predefinedFormat, String defaultValue) {
		return timestamp != null ? DateTimeFormat.getFormat(predefinedFormat).format(timestamp) : defaultValue;
	}

	/**
	 * Convert a Timestamp to String. Returns the given default value if Timestamp is null.
	 * 
	 * @param timestamp      the timestamp to convert to string
	 * @param dateTimeFormat the datetime format
	 * @return the date string, or an empty string if timestamp is null
	 */
	public static String toString(Timestamp timestamp, String dateTimeFormat, String defaultValue) {
		return timestamp != null ? DateTimeFormat.getFormat(dateTimeFormat).format(timestamp) : defaultValue;
	}

	/**
	 * Returns the result of calling toString on the given string if not null and returns an empty string otherwise.
	 * 
	 * @param string the text to process
	 * @return the result of calling toString if the text is not null or an empty String otherwise.
	 */
	public static String toString(String string) {
		return Objects.toString(string, "");
	}

	/**
	 * Convert a Boolean to String.
	 * 
	 * @param value    the value to convert
	 * @param trueStr  the String to return if value is true
	 * @param falseStr the String to return if value is false
	 * @param elseStr  the String to return if value is neither true or false
	 * @return the Boolean conversion to the given strings, according to the Boolean value
	 */
	public static String toString(Boolean value, String trueStr, String falseStr, String elseStr) {
		if (Boolean.TRUE.equals(value)) {
			return trueStr;
		} else if (Boolean.FALSE.equals(value)) {
			return falseStr;
		} else {
			return elseStr;
		}
	}
}
