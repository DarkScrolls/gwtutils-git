package com.ebalde.gwtutils.client.utils.autocomplete;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;

public class AutocompleteSuggestBox extends Composite {

	@UiField
	HTMLDivElement autocompleteList;

	private static AutocompleteSuggestBoxUiBinder uiBinder = GWT.create(AutocompleteSuggestBoxUiBinder.class);

	interface AutocompleteSuggestBoxUiBinder extends UiBinder<Widget, AutocompleteSuggestBox> {
	}

	public AutocompleteSuggestBox() {
		initWidget(uiBinder.createAndBindUi(this));
		autocompleteList.id = HTMLPanel.createUniqueId();
	}

	public HTMLElement getElemental2() {
		return autocompleteList;
	}

}
