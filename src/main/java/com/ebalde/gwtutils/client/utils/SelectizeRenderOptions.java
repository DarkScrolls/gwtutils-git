package com.ebalde.gwtutils.client.utils;

import com.ebalde.gwtutils.client.utils.SelectizeOptions.OptionRenderer;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = JsPackage.GLOBAL, isNative = true, name = "Object")
public class SelectizeRenderOptions {
	@JsProperty
	public OptionRenderer option;

	@JsProperty
	public OptionRenderer item;
}