package com.ebalde.gwtutils.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import elemental2.dom.HTMLDivElement;

public class ToastPanel extends Composite {
	
	@UiField
	HTMLDivElement toastContainer;

	private static ToastPanelUiBinder uiBinder = GWT.create(ToastPanelUiBinder.class);

	interface ToastPanelUiBinder extends UiBinder<Widget, ToastPanel> {
	}

	public ToastPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		toastContainer.id = "toast-container";
	}
}
