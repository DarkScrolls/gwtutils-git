package com.ebalde.gwtutils.client.utils.autocomplete;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLAnchorElement;
import elemental2.dom.HTMLInputElement;

public class AutocompleteString extends Autocomplete<String> {

	public AutocompleteString(HTMLInputElement htmlInputElement, AutoCompleteListener<String> autoCompleteListener) {
		super(htmlInputElement, autoCompleteListener);
	}

	@Override
	public String getText(String suggestion) {
		return suggestion;
	}

	@Override
	public HTMLAnchorElement getSuggestionElement(String search, String suggestion) {
		HTMLAnchorElement suggestionElement = (HTMLAnchorElement) DomGlobal.document.createElement("A");
		suggestionElement.innerHTML = getDisplayString(search, getText(suggestion));
		return suggestionElement;
	}

}
