package com.ebalde.gwtutils.client.utils;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * Boostrap 5
 * 
 * @author diogo
 *
 */
@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public class Bootstrap {
	@JsMethod(namespace = JsPackage.GLOBAL)
	public static native String activateTooltips();
}