package com.ebalde.gwtutils.client.utils.autocomplete;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.rpc.AsyncCallback;

import elemental2.dom.CSSProperties.WidthUnionType;
import elemental2.dom.Element;
import elemental2.dom.Event;
import elemental2.dom.HTMLAnchorElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.KeyboardEvent;

/**
 * Turn a {@link HTMLInputElement} into an auto-complete search input with suggestion lazy loading and the possibility
 * to define the suggestion type and display.
 * 
 * @author diogo
 *
 */
public abstract class Autocomplete<T> {
	private HTMLInputElement htmlInputElement; // required
	private AutoCompleteListener<T> autoCompleteListener; // required
	private int maxSuggestions = 10; // optional
	private boolean isSingleItemSelect; // optional
	private T selected;
	private int activeIndex = -1;
	private HTMLElement autocompleteSuggestBox = new AutocompleteSuggestBox().getElemental2();
	private ArrayList<T> suggestions;
	private SelectListener<T> selectListener;
	/**
	 * a timestamp for each request, to ignore any responses that don't have the highest timestamp seen so far.
	 */
	private long lastRequestTimestamp = 0;

	public Autocomplete(HTMLInputElement htmlInputElement, AutoCompleteListener<T> autoCompleteListener) {
		super();
		this.htmlInputElement = htmlInputElement;
		this.autoCompleteListener = autoCompleteListener;

		// add div that will contain the suggestions to the input parent
		htmlInputElement.parentElement.appendChild(autocompleteSuggestBox);

		addKeyboardEventListener(htmlInputElement);

		// close the suggestion box if focus on the input is lost
		htmlInputElement.addEventListener("focusout", evt -> {
			autocompleteSuggestBox.classList.add("d-none");
			if (isSingleItemSelect && selected == null) {
				htmlInputElement.value = "";
			}
		});
	}

	private void getSearchSuggestions(AutoCompleteListener<T> autoCompleteListener) {
		if (!htmlInputElement.value.isEmpty()) {
			final long requestTimestamp = System.currentTimeMillis();
			lastRequestTimestamp = requestTimestamp;

			// suggestion htmlAutocompleteList shall have the same width as the input
			autocompleteSuggestBox.style.width = WidthUnionType.of(htmlInputElement.offsetWidth + "px");

			autocompleteSuggestBox.classList.remove("d-none"); // in case it had been closed

			autoCompleteListener.getSearchSuggestions(htmlInputElement.value, new AsyncCallback<ArrayList<T>>() {

				@Override
				public void onSuccess(ArrayList<T> result) {
					if (requestTimestamp < lastRequestTimestamp) {
						return; // Discard outdated result
					}
					Autocomplete.this.suggestions = result;
					selected = null;
					autocompleteSuggestBox.innerHTML = ""; // clean the list
					addSuggestionElements(result);
					activateSuggestion();
				}

				@Override
				public void onFailure(Throwable caught) {
				}
			});
		} else {
			autocompleteSuggestBox.innerHTML = ""; // clean the list - helpful when the user erased the input text
		}
	}

	private void addKeyboardEventListener(HTMLInputElement htmlInputElement) {
		/*
		 * key down (instead of key up) for Escape and Enter, before these events are caught by other methods, like
		 * close modal.
		 */
		htmlInputElement.addEventListener("keydown", evt -> {
			KeyboardEvent keyboardEvent = (KeyboardEvent) evt;
			if (keyboardEvent.key.equals("ArrowDown")) {
				activeIndex++;
				activateSuggestionAndPreventEventPropagation(evt);
			} else if (keyboardEvent.key.equals("ArrowUp")) {
				activeIndex--;
				activateSuggestionAndPreventEventPropagation(evt);
			} else if (keyboardEvent.key.equals("Enter")) {
				if (activeIndex >= 0 && activeIndex < autocompleteSuggestBox.childNodes.length) {
					selectSuggestion(suggestions.get(activeIndex));
				} else if (!isSingleItemSelect) {
					// no item selected, but enter pressed (e.g. search for input entered, not selection)
					selectSuggestion(null);
				}
				evt.preventDefault();
				evt.stopPropagation();
				activeIndex = -1;
			} else if (keyboardEvent.key.equals("Escape")) {
				// pressing Escape should only undo the action on this input
				htmlInputElement.value = "";
				autocompleteSuggestBox.innerHTML = "";
				evt.preventDefault();
				evt.stopPropagation();
			} else if (isSingleItemSelect && getSelected() != null && (keyboardEvent.key.equals("Backspace"))) {
				// erase complete selection
				selected = null;
				htmlInputElement.value = "";
				return; // don't get search suggestions
			}
		});
		/*
		 * Key up for all other keys, so that the input value is already updated when this event triggers, unlike it
		 * happens with key down.
		 */
		htmlInputElement.addEventListener("keyup", evt -> {
			KeyboardEvent keyboardEvent = (KeyboardEvent) evt;
			if (keyboardEvent.key.equals("ArrowDown") || keyboardEvent.key.equals("ArrowUp")
					|| keyboardEvent.key.equals("Enter") || keyboardEvent.key.equals("Escape")) {
				return; // don't get search suggestions
			} else {
				getSearchSuggestions(autoCompleteListener);
			}
		});
	}

	/**
	 * Activate the suggestion and prevent the event propagation.
	 * 
	 * @param evt the event to prevent propagation
	 */
	private void activateSuggestionAndPreventEventPropagation(Event evt) {
		activateSuggestion();
		evt.preventDefault();
		evt.stopPropagation();
	}

	private void addSuggestionElements(ArrayList<T> result) {
		int numSuggestions = 0;
		for (T suggestion : result) {
			HTMLAnchorElement suggestionElement = getSuggestionElement(htmlInputElement.value, suggestion);
			suggestionElement.href = "javascript:;";
			suggestionElement.classList.add("list-group-item");
			suggestionElement.classList.add("list-group-item-action");
			suggestionElement.setAttribute("data-value", getText(suggestion));
			// mousedown event is always triggered before the focusout event while click is not.
			suggestionElement.addEventListener("mousedown", evt1 -> selectSuggestion(suggestion));
			autocompleteSuggestBox.appendChild(suggestionElement);
			if (++numSuggestions >= getMaxSuggestions()) {
				return;
			}
		}
	}

	/**
	 * Defines the suggestion element, which can be designed depending on the suggestion type.
	 * 
	 * @param suggestion the suggestion
	 * @return the suggestion HTML element
	 */
	public abstract HTMLAnchorElement getSuggestionElement(String search, T suggestion);

	private void activateSuggestion() {
		for (int i = 0; i < autocompleteSuggestBox.childNodes.length; i++) {
			Element selected = (HTMLElement) autocompleteSuggestBox.childNodes.getAt(i);
			selected.classList.remove("list-group-item-secondary");
		}
		if (activeIndex >= autocompleteSuggestBox.childNodes.length) {
			activeIndex = 0;
		} else if (activeIndex < -1) {
			activeIndex = (autocompleteSuggestBox.childNodes.length - 1);
		}
		if (activeIndex != -1 && activeIndex < autocompleteSuggestBox.childNodes.length) {
			Element selected = (HTMLElement) autocompleteSuggestBox.childNodes.getAt(activeIndex);
			selected.classList.add("list-group-item-secondary");
		}
	}

	/**
	 * Set a suggestion as selected. Adds its value to the input and closes the suggestionsBox.
	 * 
	 * @param suggestion the suggestion to select
	 */
	private void selectSuggestion(T suggestion) {
		selected = suggestion;
		if (suggestion != null) {
			htmlInputElement.value = getText(suggestion);
		}
		autocompleteSuggestBox.classList.add("d-none");
		if (selectListener != null) {
			selectListener.onSelect(suggestion);
		}
	}

	/**
	 * Get the suggestion formatted String
	 * 
	 * @param search           the search String
	 * @param suggestionString the suggestion String
	 * @return a formatted suggestion String
	 */
	protected String getDisplayString(String search, String suggestionString) {
		String[] searchWords = search.split("\\s");
		Arrays.sort(searchWords, new java.util.Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s2.length() - s1.length();// comparison
			}
		}); // long strings should match first - match "word" instead of "wo" if both are in the searchWords
		String pattern = "(";
		for (int i = 0; i < searchWords.length; i++) {
			pattern += searchWords[i];
			if (i < searchWords.length - 1) { // not the last
				pattern += "|";
			}
		}
		pattern += ")";

		return getFormattedSuggestion(suggestionString, pattern);
	}

	/**
	 * Get the text that will be displayed on the input element when the suggestion is selected.
	 * 
	 * @param suggestion the suggestion
	 * @return the text that shall be displayed on the input when the suggestion is selected
	 */
	public abstract String getText(T suggestion);

	/**
	 * Get the formatted suggestion suggestion text
	 * 
	 * @param suggestionText the suggestion text to format
	 * @param pattern        the search pattern, which includes the matches of every word in the search
	 * @return the formatted suggestion
	 */
	private String getFormattedSuggestion(String suggestionText, String pattern) {
		// Create a case-insensitive regular expression object
		RegExp regExp = RegExp.compile(pattern, "i");

		// Define the replacement string, using $& to refer to the entire matched text
		String replacement = "<strong class=\"bg-light\">$&</strong>";

		// Use the replace method to replace all occurrences of the pattern
		String result = regExp.replace(suggestionText, replacement);

		return result;
	}

	public T getSelected() {
		return selected;
	}

	public void setSelected(T selected) {
		this.selected = selected;
		selectSuggestion(selected);
	}

	public int getMaxSuggestions() {
		return maxSuggestions;
	}

	public void setMaxSuggestions(int maxSuggestions) {
		this.maxSuggestions = maxSuggestions;
	}

	public boolean isSingleItemSelect() {
		return isSingleItemSelect;
	}

	public void setSingleItemSelect(boolean isSingleItemSelect) {
		this.isSingleItemSelect = isSingleItemSelect;
	}

	public SelectListener<T> getSelectListener() {
		return selectListener;
	}

	public void setSelectListener(SelectListener<T> selectListener) {
		this.selectListener = selectListener;
	}

}
