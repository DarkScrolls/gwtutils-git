package com.ebalde.gwtutils.client.utils;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.ebalde.gwtutils.client.utils.autocomplete.Autocomplete;
import com.ebalde.gwtutils.shared.DateVariablePrecision;
import com.ebalde.gwtutils.shared.EnumDatePrecision;
import com.ebalde.gwtutils.shared.FieldVerifier;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLSelectElement;
import elemental2.dom.HTMLTextAreaElement;
import elemental2.dom.KeyboardEvent;

public class HTMLElementUtils {

	/**
	 * Converts the value of an input element to Integer
	 * 
	 * @param htmlInputElement the input element
	 * @return the Integer value of an input element, null if empty or not a number
	 */
	public static Integer toInteger(HTMLInputElement htmlInputElement) {
		if (!FieldVerifier.isValidNumber(htmlInputElement.value)) {
			return null;
		}
		return htmlInputElement.value.isEmpty() ? null : Integer.valueOf(htmlInputElement.value);
	}

	/**
	 * Converts the value of a select element to Integer
	 * 
	 * @param htmlSelectElement the input element
	 * @return the Integer value of a select element, null if empty or not a number
	 */
	public static Integer toInteger(HTMLSelectElement htmlSelectElement) {
		if (!FieldVerifier.isValidNumber(htmlSelectElement.value)) {
			return null;
		}
		return htmlSelectElement.value.isEmpty() ? null : Integer.valueOf(htmlSelectElement.value);
	}

	/**
	 * Gets the data-id of the selected radio button as Integer
	 * 
	 * @param radioGroupName the radio group name
	 * @return the Integer data-id of the checked element, null if empty or not a number
	 */
	public static Integer toInteger(String radioGroupName) {
		List<Element> elements = DomGlobal.document.getElementsByName(radioGroupName).asList();
		Integer checkedElementDataId = null;
		for (Element element : elements) {
			if (element instanceof HTMLInputElement && ((HTMLInputElement) element).checked
					&& FieldVerifier.isValidNumber(element.getAttribute("data-id"))) {
				checkedElementDataId = Integer.valueOf(element.getAttribute("data-id"));
				break;
			}
		}
		return checkedElementDataId;
	}

	/**
	 * Gets the data-id of the selected checkbox as a list of Integer
	 * 
	 * @param checkboxGroupName the checkbox group name
	 * @return the list of Integer data-id of the checked elements
	 */
	public static List<Integer> toIntegerList(String checkboxGroupName) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		DomGlobal.document.body.querySelectorAll("input[type=checkbox][name=" + checkboxGroupName + "]:checked")
				.asList().stream().forEach(t -> {
					if (FieldVerifier.isValidNumber(t.getAttribute("data-id"))) {
						ids.add(Integer.valueOf(t.getAttribute("data-id")));
					}
				});
		return ids;
	}

	/**
	 * Converts the value of an HTML input element to Date
	 * 
	 * @param htmlSelectElement the input element
	 * @param predefinedFormat  the date format
	 * @return the Date value of an input element, null if empty
	 */
	public static Date toDate(HTMLInputElement htmlInputElement, PredefinedFormat predefinedFormat) {
		return htmlInputElement.value.isEmpty() ? null
				: new Date(DateTimeFormat.getFormat(predefinedFormat).parse(htmlInputElement.value).getTime());
	}

	/**
	 * Converts the value of an HTML input element to Timestamp
	 * 
	 * @param htmlSelectElement the input element
	 * @param predefinedFormat  the date format
	 * @return the Timestamp value of an input element, null if empty
	 */
	public static Timestamp toTimestamp(HTMLInputElement htmlInputElement, PredefinedFormat predefinedFormat) {
		Date date = new Date(DateTimeFormat.getFormat(predefinedFormat).parse(htmlInputElement.value).getTime());
		return htmlInputElement.value.isEmpty() ? null : new Timestamp(date.getTime());
	}

	/**
	 * Converts the value of the HTML input day, month and year elements to Date.
	 * 
	 * @param dayInput   the day HTML input element
	 * @param monthInput the month HTML input element
	 * @param yearInput  the year HTML input element
	 * @return the date based on the value of the HTML input day, month and year elements
	 */
	public static DateVariablePrecision toDateVariablePrecision(HTMLInputElement dayInput, HTMLInputElement monthInput,
			HTMLInputElement yearInput) {
		Date date = null;
		String dd = dayInput.value.isEmpty() ? "01" : dayInput.value;
		String mm = monthInput.value.isEmpty() ? "01" : monthInput.value;
		if (!yearInput.value.isEmpty()) { // at least the year must be entered
			date = new Date(DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM)
					.parse(dd + "." + mm + "." + yearInput.value).getTime());
		}

		EnumDatePrecision enumDatePrecision = EnumDatePrecision.DAY;
		if (!dayInput.value.isEmpty() && !monthInput.value.isEmpty() && !yearInput.value.isEmpty()) {
			enumDatePrecision = EnumDatePrecision.DAY;
		} else if (!monthInput.value.isEmpty() && !yearInput.value.isEmpty()) {
			enumDatePrecision = EnumDatePrecision.MONTH;
		} else if (!yearInput.value.isEmpty()) {
			enumDatePrecision = EnumDatePrecision.YEAR;
		}

		return new DateVariablePrecision(date, enumDatePrecision);
	}

	/**
	 * Converts the value of an input element to String
	 * 
	 * @param htmlInputElement the input element
	 * @return the String value of an input element, null if empty
	 */
	public static String toString(HTMLInputElement htmlInputElement) {
		return htmlInputElement.value.isEmpty() ? null : htmlInputElement.value;
	}

	/**
	 * Converts the value of an input element to String
	 * 
	 * @param HTMLTextAreaElement the input element
	 * @return the String value of an input element, null if empty
	 */
	public static String toString(HTMLTextAreaElement htmlInputElement) {
		return htmlInputElement.value.isEmpty() ? null : htmlInputElement.value;
	}

	/**
	 * Converts a yes/no 2-radio input elements into a boolean
	 * 
	 * @param yes the radio input representing yes
	 * @param no  the radio input representing no
	 * @return the Boolean value of the yes/no radio input elements
	 */
	public static Boolean toBoolean(HTMLInputElement yes, HTMLInputElement no) {
		if (yes.checked)
			return true;
		else if (no.checked)
			return false;
		else
			return null;
	}

	/**
	 * Converts the value of an input element to Float
	 * 
	 * @param htmlInputElement the input element
	 * @return the Float value of an input element, null if empty or not a number
	 */
	public static Float toFloat(HTMLInputElement htmlInputElement) {
		if (!FieldVerifier.isValidFloat(htmlInputElement.value)) {
			return null;
		}
		return htmlInputElement.value.isEmpty() ? null : Float.valueOf(htmlInputElement.value);
	}

	/**
	 * Converts the value of an input element to BigDecimal
	 * 
	 * @param htmlInputElement the input element
	 * @return the BigDecimal value of an input element, null if empty or not a number
	 */
	public static BigDecimal toBigDecimal(HTMLInputElement htmlInputElement) {
		String value = htmlInputElement.value.replace(",", "."); // in case user enters ',' for decimal, like in Germany
		if (!FieldVerifier.isValidFloat(value)) {
			return null;
		}
		return value.isEmpty() ? null : new BigDecimal(value);
	}

	/**
	 * Converts the value of an input element to Long
	 * 
	 * @param htmlInputElement the input element
	 * @return the Long value of an input element, null if empty or not a number
	 */
	public static Long toLong(HTMLInputElement htmlInputElement) {
		if (!FieldVerifier.isValidNumber(htmlInputElement.value)) {
			return null;
		}
		return htmlInputElement.value.isEmpty() ? null : Long.valueOf(htmlInputElement.value);
	}

	/**
	 * When a date input value is edited, "." is added automatically according to the dd.mm.yyyy format
	 * 
	 * @param htmlInputElement the input element for which the auto-fill will be set
	 */
	public static void setDateInputAutofill(HTMLInputElement htmlInputElement) {
		htmlInputElement.addEventListener("keydown", evt -> {
			KeyboardEvent keyboardEvent = (KeyboardEvent) evt;
			int l = htmlInputElement.value.length();
			if ((l == 2 || l == 5) && Character.isDigit(htmlInputElement.value.charAt(l - 1))
					&& keyboardEvent.key != "Backspace" && keyboardEvent.key != ".") {
				htmlInputElement.value = htmlInputElement.value + ".";
			}
		});
	}

	/**
	 * Converts the selected object from the provided Autocomplete instance to an ID - Integer. The conversion is done
	 * using the provided mapper function.
	 *
	 * @param <T>          The type of the selected object in the Autocomplete instance.
	 * @param autocomplete The Autocomplete instance from which the selected object is retrieved.
	 * @param mapper       The function that maps the selected object to an Integer.
	 * @return The Integer representation of the selected object, or null if the selected object is null.
	 */
	public static <T> Integer toInteger(Autocomplete<T> autocomplete, Function<T, Integer> mapper) {
		T selectedObject = autocomplete.getSelected();
		return selectedObject != null ? mapper.apply(selectedObject) : null;
	}

}
