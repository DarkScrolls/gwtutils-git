package com.ebalde.gwtutils.client.utils;
import elemental2.dom.Element;
import jsinterop.annotations.JsType;
import jsinterop.annotations.JsPackage;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "bootstrap.Tooltip")
public class Tooltip {

    public Tooltip(Element element, Options options) {
    }

    public native void show();

    public native void hide();

    public native void update();

    public native void dispose();

    public native void toggle();

    public native void enable();

    public native void disable();

    public native boolean toggleEnabled();

    @JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
    public static class Options {
        public String placement; // 'top', 'right', 'bottom', 'left', 'auto'
        public String title;
        public boolean html;
        public String template;
        public String trigger; // 'click', 'hover', 'focus', 'manual'
        public int delay;
        public String container;
        public boolean boundary;
        public boolean sanitize;
        public Object whiteList; // You can define a more specific type if needed
        public Object popperConfig; // You can define a more specific type if needed
        // Add other options as needed
    }
}