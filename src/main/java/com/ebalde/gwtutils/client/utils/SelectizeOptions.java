package com.ebalde.gwtutils.client.utils;

import java.util.function.Function;

import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = JsPackage.GLOBAL, isNative = true, name = "Object")
public class SelectizeOptions {

	@JsProperty
	public String[] plugins;

    @JsProperty
    public FunctionCreateParam create;
    
    @JsProperty
    public SelectizeRenderOptions render;

	@FunctionalInterface
	@JsFunction
	public interface FunctionCreateParam {
		SelectizeCreateParams create(String input);
	}
	
	@FunctionalInterface
	@JsFunction
	public interface OptionRenderer {
	    String render(SelectizeOption data, Function<String, String> escape);
	}
}