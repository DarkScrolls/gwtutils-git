package com.ebalde.gwtutils.client.utils;

import com.google.gwt.user.client.ui.HTMLPanel;

import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLLabelElement;
import elemental2.dom.Node;

public class FormInputUtils {

	/**
	 * Sets the input id of input, textarea and select to the value of its 'name' or 'aria-label' attribute.
	 * 
	 * @param parent the {@link HTMLElement} ancestor element for which all children input elements will be processed
	 */
	public static void setInputIdEqName(HTMLElement parent) {
		// solution for the label.for -> input.id link (elements cannot have ui:field and id attributes)
		for (Node n : parent.querySelectorAll("input,textarea,select").asList()) {
			if (n instanceof HTMLElement && ((HTMLElement) n).hasAttribute("name")) {
				((HTMLElement) n).id = ((HTMLElement) n).getAttribute("name");
			}
		}
	}

	/**
	 * Sets the input id of input, textarea and select to the value of its 'data-id' attribute.
	 * 
	 * @param parent the {@link HTMLElement} ancestor element for which all children input elements will be processed
	 */
	public static void setInputIdEqDataid(HTMLElement parent) {
		// solution for the label.for -> input.id link (elements cannot have ui:field and id attributes)
		for (Node n : parent.querySelectorAll("input,textarea,select").asList()) {
			if (n instanceof HTMLElement && ((HTMLElement) n).hasAttribute("data-id")) {
				((HTMLElement) n).id = ((HTMLElement) n).getAttribute("data-id");
			}
		}
	}

	/**
	 * Assigns a unique ID to the given HTMLInputElement and associates the given HTMLLabelElement with it. The 'for'
	 * attribute of the label is set to the unique ID of the input element, establishing a relationship between the
	 * label and the input element for better accessibility and user experience.
	 *
	 * @param element The HTMLInputElement to which a unique ID will be assigned.
	 * @param label   The HTMLLabelElement that will be associated with the input element.
	 */
	public static void setUniqueIdAndLabel(HTMLElement element, HTMLLabelElement label) {
		String uniqueId = HTMLPanel.createUniqueId(); // Generate a unique ID
		element.id = uniqueId; // Assign the unique ID to the input element
		label.setAttribute("for", uniqueId); // Associate the label with the input element
	}

	/**
	 * Assigns a unique ID to the given HTMLInputElement. This method is useful for elements that do not require a label
	 * association.
	 *
	 * @param element The HTMLInputElement to which a unique ID will be assigned.
	 */
	public static void setUniqueId(HTMLInputElement element) {
		String uniqueId = HTMLPanel.createUniqueId(); // Generate a unique ID
		element.id = uniqueId; // Assign the unique ID to the input element
	}
}
