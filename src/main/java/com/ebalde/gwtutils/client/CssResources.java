package com.ebalde.gwtutils.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface CssResources extends ClientBundle {
  public static final CssResources INSTANCE =  GWT.create(CssResources.class);

  @Source("modal.css")
  public TextResource modalCss();
  
  @Source("gwt-utils.css")
  public TextResource gwtUtils();
}