package com.ebalde.gwtutils.client.page;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HistoryTokenManagerTest {

    @Test
    public void historyTokenPartsTest() {
        String historyToken = "Search/mySearchInput/otherInput";

        // assert statements
        assertEquals("Search", HistoryTokenManager.getPageName(historyToken));
        assertEquals("Search", HistoryTokenManager.getParameter(historyToken, 0));
        assertEquals("mySearchInput", HistoryTokenManager.getParameter(historyToken, 1));
        assertEquals("otherInput", HistoryTokenManager.getParameter(historyToken, 2));
        assertEquals(null, HistoryTokenManager.getParameter(historyToken, 3));
    }
}