package com.ebalde.gwtutils.shared;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FieldVerifierTest {

    @Test
    public void testIsValidPMID() {
        // Valid PMIDs
        assertTrue(FieldVerifier.isValidPMID("12345678"));
        assertTrue(FieldVerifier.isValidPMID("1"));
        assertTrue(FieldVerifier.isValidPMID("123"));

        // Invalid PMIDs
        assertFalse(FieldVerifier.isValidPMID("123456789")); // More than 8 digits
        assertFalse(FieldVerifier.isValidPMID("")); // Empty string
        assertFalse(FieldVerifier.isValidPMID("abc123")); // Contains non-digit characters
        assertFalse(FieldVerifier.isValidPMID("330785829"));
    }

    @Test
    public void testIsValidDOI() {
        // Valid DOIs
        assertTrue(FieldVerifier.isValidDOI("10.1000/xyz123"));
        assertTrue(FieldVerifier.isValidDOI("10.1234/abc.def.ghi"));
        assertTrue(FieldVerifier.isValidDOI("10.1000/XYZ123")); // Case-insensitive test
        assertTrue(FieldVerifier.isValidDOI("10.1200/PO.18.00105"));
        assertTrue(FieldVerifier.isValidDOI("10.3324/haematol.2022.282160"));
        assertTrue(FieldVerifier.isValidDOI("10.1111/os.12786"));

        // Invalid DOIs
        assertFalse(FieldVerifier.isValidDOI("10.1000")); // Missing suffix
        assertFalse(FieldVerifier.isValidDOI("abc/xyz123")); // Missing prefix
        assertFalse(FieldVerifier.isValidDOI("")); // Empty string
        assertFalse(FieldVerifier.isValidDOI("https://doi.org/10 .1200/JCO.23.020 05"));
        assertFalse(FieldVerifier.isValidDOI("DOI:10.1200/JCO.202 0.38.15_suppl.10508"));
        assertFalse(FieldVerifier.isValidDOI("https://doi.org/10.1 200/JCO.2021.39. 15_suppl.2007"));
        assertFalse(FieldVerifier.isValidDOI("https://doi.org/10.11 11/jcpt.12378"));
        assertFalse(FieldVerifier.isValidDOI("https://doi.org/10.1200/J CO.2022.40.16_suppl.40 0"));
        assertFalse(FieldVerifier.isValidDOI("10.1200/JCO.2023.41.1 6_suppl.3501"));
    }
}
